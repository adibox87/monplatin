<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalonsSalonServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salons_salon_services', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('salon_id')->unsigned()->index();
            $table->foreign('salon_id')->references('id')->on('salons')->onDelete('cascade');
            $table->unsignedBigInteger('salon_service_id')->unsigned()->index();
            $table->foreign('salon_service_id')->references('id')->on('salon_services')->onDelete('cascade');;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('salons_salon_services', function (Blueprint $table) {
            Schema::dropIfExists('salons_salon_services');
        });
    }
}
