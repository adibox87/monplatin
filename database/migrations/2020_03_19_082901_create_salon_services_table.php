<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalonServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salon_services', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('salon_service_category_id')->unsigned()->index();
            $table->foreign('salon_service_category_id')->references('id')->on('salon_service_categories')->onDelete('cascade');
            $table->string('title');
            $table->string('slug');
            $table->text('small_description')->nullable();
            $table->text('description')->nullable();
            $table->float('price');
            $table->string('image')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salon_services');
    }
}
