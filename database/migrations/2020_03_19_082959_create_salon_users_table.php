<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalonUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salon_users', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('salon_id')->unsigned()->index();
            $table->foreign('salon_id')->references('id')->on('salons')->onDelete('cascade');
            $table->string('name');
            $table->string('slug');
            $table->string('position')->nullable();
            $table->string('image')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salon_users');
    }
}
