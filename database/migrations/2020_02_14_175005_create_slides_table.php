<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSlidesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('slides', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('slider_id')->unsigned()->index();
            $table->foreign('slider_id')->references('id')->on('sliders')->onDelete('cascade');
            $table->string('url')->nullable();
            $table->string('title')->nullable();
            $table->string('text_button')->nullable();
            $table->text('text')->nullable();
            $table->integer('order')->increments()->unsigned()->default(0);
            $table->string('image')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('slides');
    }
}
