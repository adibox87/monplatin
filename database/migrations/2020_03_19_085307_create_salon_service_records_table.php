<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalonServiceRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salon_service_records', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('salon_service_id')->unsigned()->index();
            $table->foreign('salon_service_id')->references('id')->on('salon_services')->onDelete('cascade');
            $table->string('name');
            $table->string('phone')->nullable();
            $table->dateTime('date_time')->nullable();
            $table->boolean('active')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salon_service_records');
    }
}
