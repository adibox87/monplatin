<div id="hb-footer" class="hb-footer hb-haslayout">
    <div class="hb-footer-area">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-lg-4">
                    <div class="hb-col">
                        <strong class="hb-logo">
                            <a href="{{ route('home') }}"><img src="{{ url('images/logo.png') }}" alt=""></a>
                        </strong>
                        <span class="hb-timeandday">Фирменный салон-магазин</span>
                        <ul class="list-unstyled hb-socialicons">
                            <li><a target="_blank" href="{{ setting('site.fb', 'fb') }}"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a target="_blank" href="{{ setting('site.ok', 'ok') }}"><i class="fab fa-odnoklassniki"></i></a></li>
                            <li><a target="_blank" href="{{ setting('site.instagram', 'instagram') }}"><i class="fab fa-instagram"></i></a></li>
                            <li><a target="_blank" href="{{ setting('site.vk', 'vk') }}"><i class="fab fa-vk"></i></a></li>
                        </ul>
                    </div>
                </div>
                @foreach($salons as $salon)
                    <div class="col-xs-12 col-md-6 col-lg-4">
                        <div class="hb-col">
                            <h3><a href="{{ route('salon.view', $salon->slug) }}">{{ $salon->title }}</a></h3>
                            <ul class="list-unstyled hb-info">
                                <li>
                                    <span>
                                        Время работы:
                                        <em>{!! nl2br($salon->work_time) !!}</em>
                                    </span>
                                </li>
                                <li>
                                    <span>
                                        Телефоны:
                                        @foreach(explode("\n", $salon->phones) as $phone)
                                            <a href="tel:{{ preg_replace('/\D/', '', $phone) }}">
                                                {{ $phone }}
                                            </a>
                                        @endforeach
                                    </span>
                                </li>
                            </ul>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="hb-footerbar">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <span class="hb-copyright" style="font-size: 14px; line-height: 17px;">
                        &copy; {{ now()->year }} Разработка:
                        <a target="_blank" href="https://devolt.one/" style="font-family: Montserrat,sans-serif;font-weight: 900;">D.1</a>
                        x
                        <a target="_blank" href="https://huskymedia.by" style="font-family: 'Nunito Sans', sans-serif; font-weight: 800">huskymedia</a></span>
                    <ul class="list-unstyled hb-footernav">
                        <li><a href="{{ url('about') }}">О нас </a></li>
                        <li><a href="{{ url('dostavka') }}">Доставка </a></li>
                        <li><a href="{{ url('contacts') }}">Контакты</a></li>
                        <li><a href="{{ url('diskontnaya-programma') }}">Дисконтная программа</a></li>
                        <li><a href="{{ url('podarochnye-sertifikaty') }}">Подарочные сертификаты</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
