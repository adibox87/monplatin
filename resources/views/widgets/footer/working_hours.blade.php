@foreach($salons as $salon)
    <a href="{{ route('salon.view', $salon->slug) }}">{{ $salon->title }}</a> <br />
    <span class="hb-timeandday mt-10">{!! nl2br($salon->work_time) !!}</span>
@endforeach
