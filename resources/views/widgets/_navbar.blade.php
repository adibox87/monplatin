<div id="hb-header" class="hb-header v2 hb-haslayout">
    <div class="hb-topbar">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <strong class="hb-logo"><a href="{{ route('home') }}"><img src="{{ url('images/logo.png') }}" alt="company logo here"></a></strong>
                    <div class="hb-addnav">
                        <div class="hb-cartarea">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                                <div class="cart-wrapper">
                                    <span class="cart-quantity">{{count(Cart::getContent())}}</span>
                                    <span class="icon_cart" style="font-size: 2.5rem;"></span>
                                </div>
                            </a>
                            <ul class="list-unstyled dropdown-menu hb-cart right">
                                @foreach($cart = Cart::getContent() as $cartItem)
                                    <li>
                                        @if($cartItem->image)
                                            <figure><img src="/storage/{{ $cartItem->image }}" alt="{{ $cartItem->name }}"></figure>
                                        @endif
                                        <h3><span class="text-black"><b>{{ $cartItem->price }}</b> x <b>{{ $cartItem->quantity }}</b></span>{{ $cartItem->name }}</h3>
                                        <a class="remove" href="{{ route('cart.delete', $cartItem->id) }}"><i class="ti-close"></i></a>
                                    </li>
                                @endforeach
                                <li>
                                    <span class="hb-total text-black">Итого:<em><b>{{ Cart::getTotal() }}</b></em></span>
                                </li>
                                <li>
                                    <a href="{{ route('cart.index') }}" class="hb-btn hb-btn-lg cart-btn changehover font-base fw-medium text-center text-uppercase">В корзину</a>
                                    <a href="{{ route('checkout.index') }}" class="hb-btn hb-btn-lg cart-btn font-base fw-medium text-center text-uppercase">Оформить</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <a href="#" data-modal="#modal-contacts" class="hb-btn hidden-xs modal__trigger">Связаться</a>
                    <div class="hb-info-area">
                        <ul class="list-unstyled hb-info">
                            @foreach($salons as $salon)
                                <li>
                                    <i class="ti-location-pin hidden-xs hidden-sm"></i>
                                    <span>
                                        {{ $salon->title }}
                                        @foreach(explode("\n", $salon->phones) as $phone)
                                            <a href="tel:{{ preg_replace('/\D/', '', $phone) }}" class="@if(!$loop->first) hidden-xs @endif">
                                                {{ $phone }}
                                            </a>
                                        @endforeach
                                    </span>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="hb-navigationarea">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="hb-addnav">
                        <div class="hb-searcharea">
                            <form action="{{ route('search.index') }}">
                                <input type="text" placeholder="Поиск" name="search" class="hb-emailarea hb-formtheme form-control">
                            </form>
                        </div>
                    </div>
                    <nav id="hb-nav" class="hb-nav">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#hb-navigation" aria-expanded="false">
                                <span class="sr-only">Навигация</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <div id="hb-navigation" class="collapse navbar-collapse hb-navigation">
                            <ul>
                                <li><a href="{{ route('home') }}">Главная </a></li>
                                <li class="menu-item-has-children">
                                    <a href="{{ route('salon.index') }}">Салоны красоты</a>
                                    <ul class="list-unstyled sub-menu">
                                        @foreach($salons as $salon)
                                            <li><a href="/salons/{{ $salon->slug }}">{{ $salon->title }}</a></li>
                                        @endforeach
                                    </ul>
                                </li>
                                <li><a href="{{ route('catalog.index') }}">Магазин </a></li>
                                <li><a href="{{ route('article.articles', ['category' => 'blog']) }}">Блог </a></li>
                                <li class="menu-item-has-children">
                                    <a href="{{ route('article.categories') }}">Акции</a>
                                    <ul class="list-unstyled sub-menu">
                                        <li><a href="{{ route('article.articles', ['category' => 'akcii-salona']) }}">Акции салона</a></li>
                                        <li><a href="{{ route('article.articles', ['category' => 'akcii-magazina']) }}">Акции магазина</a></li>
                                    </ul>
                                </li>
                                <li><a href="{{ route('portfolio.index') }}">Галерея</a></li>
                                <li><a href="{{ url('about') }}">О нас</a></li>
                                <li><a href="{{ route('contact') }}">Контакты</a></li>
                                <li><a href="{{ route('customer.dashboard') }}">Личный кабинет</a></li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
        @if(session()->has('msg-success'))
            <div class="container">
                <div class="alert alert-success">
                    {{ session()->get('msg-success') }}
                </div>
            </div>
        @endif
    </div>
</div>
