<div class="search-popup">
    <div class="holder">
        <div class="block">
            <a href="#" class="close-btn"><i class="fa fa-times"></i></a>
            <form action="#" class="search-form">
                <fieldset>
                    <input type="search" class="form-control" placeholder="Поиск">
                    <button type="submit" class="btn-primary"><i class="fa fa-search"></i></button>
                </fieldset>
            </form>
        </div>
    </div>
</div>
