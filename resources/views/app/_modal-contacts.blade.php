<div id="modal-contacts" class="modal modal__bg" role="dialog" aria-hidden="true">
    <div class="modal__dialog">
        <div class="modal__content">
            <h4 style="padding: 20px; text-align: center">Связаться</h4>
            <div class="row">
                <form class="form-contact contact_form" action="{{ route('contact.store') }}" method="post"
                      id="contactFormContact">
                    <div class="col-sm-12">
                        <div class="form-group">Введите Ваше имя
                            @csrf
                            <input class="form-control" name="name" id="name_contact" type="text" placeholder="Имя"
                                   required>
                        </div>
                        <div class="form-group">Введите Ваш номер
                            <input required class="form-control" name="phone" id="number_contact" type="text"
                                   placeholder="+375-(XX)-XXX-XX-XX">
                        </div>
                        <div class="form-group">Введите Ваше сообщение
                            <textarea id="form_message_contact" class="form-control" name="message" placeholder="Сообщение"
                                      rows="6" required="required" data-error="Сообщение..."></textarea>
                            <div class="help-block with-errors color-blue"></div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group mt-lg-3">
                            <button style="cursor: pointer;" onclick="form_submitContact()" type="submit"
                                    class="hb-btn button-contactFormСontact">Связаться
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- modal close button -->
            <a href="" class="modal__close demo-close">
                <svg class="" viewBox="0 0 24 24">
                    <path
                        d="M19 6.41l-1.41-1.41-5.59 5.59-5.59-5.59-1.41 1.41 5.59 5.59-5.59 5.59 1.41 1.41 5.59-5.59 5.59 5.59 1.41-1.41-5.59-5.59z"/>
                    <path d="M0 0h24v24h-24z" fill="none"/>
                </svg>
            </a>

        </div>
    </div>
</div>
