<img
    src="{{ $portfolio->imageSrc }}"
    alt="{{ $portfolio->title }}"
    class="image-fluid">

<div
    id="{{ Str::slug($portfolio->title) }}"
    class="modal modal__bg"
    role="dialog"
    aria-hidden="true">
    <div class="modal__dialog portfolio-modal" style="width: 100%">
        <div class="portfolio modal__content">
            @if($portfolio->type == 'video')
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe
                        id="Youtube{{ $portfolio->id }}"
                        class="portfolio-video"
                        src="{{ $portfolio->address }}"
                        frameborder="0"
                        width="560"
                        height="315"
                        allow="accelerometer; encrypted-media; gyroscope; picture-in-picture"
                        style="float: none;"
                        allowfullscreen>
                    </iframe>
                </div>
            @elseif($portfolio->type == 'photo')
                <img
                    src="{{ Str::startsWith($portfolio->image, 'http') ? $portfolio->image : Voyager::image($portfolio->image) }}"
                    alt="{{ $portfolio->title }}"
                    class="image-fluid" />
            @endif

            <!-- modal close button -->
            <a href="" class="modal__close demo-close">
                <svg class="" viewBox="0 0 24 24">
                    <path
                        d="M19 6.41l-1.41-1.41-5.59 5.59-5.59-5.59-1.41 1.41 5.59 5.59-5.59 5.59 1.41 1.41 5.59-5.59 5.59 5.59 1.41-1.41-5.59-5.59z"/>
                    <path d="M0 0h24v24h-24z" fill="none"/>
                </svg>
            </a>
        </div>
    </div>
</div>
