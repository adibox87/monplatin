@extends('layouts.app', [
    'title' => 'Галерея'
])

@section('html-head-bottom')
    <script src="https://cdn.jsdelivr.net/gh/npr/responsiveiframe@master/dist/jquery.responsiveiframe.min.js"></script>
@endsection

@section('content')
    <!--************************************
                    Our Gallery Start
        *************************************-->
    <section id="hb-gallery" class="hb-gallery v2 hb-sectionspace hb-haslayout">
        <div class="container">
            <div class="row">
                <div class=" col-xs-12 col-sm-12 col-sm-offset-0 col-md-8 col-md-offset-2">
                    <div class="hb-sectionhead mb-50">
                        <div class="hb-sectiontitle">
                            <h2>
                                <span>Mon Platin</span>
                                Наша галерея
                            </h2>
                        </div>
                    </div>
                </div>
                <div class=" col-xs-12">
                    <div class="hb-gallery-area">
                        <div class="hb-portfolio-head hb-haslayout">
                            <ul id="hb-filterbale-nav" class="hb-filterbale-nav option-set">
                                <li>
                                    <a class="active" data-filter="*" href="javascript:void(0);">Все</a>
                                </li>
                                @foreach($portfolioCategories as $portfolioCategory)
                                    <li>
                                        <a
                                            data-filter=".{{ Str::slug($portfolioCategory->category_title) }}"
                                            href="#{{ Str::slug($portfolioCategory->category_title) }}"
                                        >
                                            {{ $portfolioCategory->category_title }}
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>

                        <div id="filter-masonry" class="hb-portfolio-content hb-haslayout">
                            @foreach($portfolios as $portfolio)
                                <div
                                    class="masonry-grid {{ Str::slug($portfolio->portfolioCategory->category_title) }}">
                                    <div class="hb-project">
                                        <figure class="hb-galleryimg">
                                            @include('portfolio._item', $portfolio)

                                            <figcaption class="hb-gallerycontent">
                                                <ul class="list-unstyled hb-roundicon">
                                                    <li
                                                        data-modal="#{{ Str::slug($portfolio->title) }}"
                                                        class="modal__trigger">
                                                        <a
                                                            href="javascript:void(0)"
                                                            class="hb-g-icon hover1">
                                                            <i class="fas fa-arrows-alt"></i>
                                                        </a>
                                                    </li>
                                                </ul>
<!--                                                <h3>{{ $portfolio->title }}</h3>-->
                                            </figcaption>
                                        </figure>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <!--************************************
                Our Gallery End
    *************************************-->
@endsection

