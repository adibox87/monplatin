@extends('layouts.app', [
    'title' => '404'
])

@section('content')

    <section id="hb-error" class="hb-error hb-sectionspace hb-haslayout">
        <div class="container">
            <div class="hb-error-area">
                <h2 class="error-code">500</h2>
                <div class="hb-errorcontent">
                    <h3 class="title">Мы упали :(</h3>
                    <p>Скоро всем исправим, приносим извинения за доставленные неудобства...</p>
                </div>
                <a href="{{ route('home') }}" class="hb-btn">Главная</a>
            </div>
        </div>
    </section>

@endsection
