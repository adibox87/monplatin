@extends('layouts.app', [
    'title' => '404'
])

@section('content')

    <section id="hb-error" class="hb-error hb-sectionspace hb-haslayout">
        <div class="container">
            <div class="hb-error-area">
                <h2 class="error-code">404</h2>
                <div class="hb-errorcontent">
                    <h3 class="title">Страница не найдена :(</h3>
                    <p>Извините, но запрашиваемая страница не найдена...</p>
                </div>
                <a href="{{ route('home') }}" class="hb-btn">Вернуться на главную</a>
            </div>
        </div>
    </section>

@endsection
