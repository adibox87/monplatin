@extends('layouts.app', [
    'title' => "$article->title - $category->name"
])

@section('breadcrumbs')
    <div id="hb-innerbanner" class="hb-innerbanner hb-haslayout">
        <div class="containers">
            <div class="hb-innerbanner-area">
                <div class="hb-bannarheading">
                    <h1>{{ $article->title }}</h1>
                </div>
                <ul class="hb-breadcrumb">
                    <li><a href="{{ route('home') }}">Главная</a></li>
                    <li><a href="{{ route('article.categories') }}">Новости и события</a></li>
                    <li><a href="{{ route('article.articles', ['category' => $article->articleCategory->slug]) }}">{{ $category->name }}</a></li>
                    <li>{{ $article->title }}</li>
                </ul>
            </div>
        </div>
    </div>
@endsection


@section('content')
    <!--************************************
                    Blog Start
        *************************************-->
    <section id="hb-blog" class="hb-blog hb-sectionspace hb-haslayout">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12">
                    <div class="hb-post-area hb-blogdetails">
                        <article class="hb-post">
                            <figure class="hb-postimage">
                                <img src="{{ \Voyager::image($article->image) }}" alt="{{ $article->title }}">
                            </figure>
                            <div class="hb-postcontent">
                                <h2>{{ $article->title }}</h2>
                                <ul class="list-unstyled hb-postmeta">
<!--                                    <li>Добавлено {{ $article->created_at->diffForHumans() }}</li>-->
                                    <li><a href="{{ route('article.articles', ['category' => $article->articleCategory->slug]) }}">{{ $category->name }}</a></li>
                                </ul>
                            </div>
                            <div class="hb-description">
                                {!! $article->description !!}
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--************************************
                Blog End
    *************************************-->
@endsection
