@extends('layouts.app', [
    'title' => 'Новости и события'
])

@section('breadcrumbs')
    <div id="hb-innerbanner" class="hb-innerbanner hb-haslayout">
        <div class="containers">
            <div class="hb-innerbanner-area">
                <div class="hb-bannarheading">
                    <h1>Новости и события</h1>
                </div>
                <ul class="hb-breadcrumb">
                    <li><a href="{{ route('home') }}">Главная</a></li>
                    <li>Новости и события</li>
                </ul>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <!--************************************
                    Blog Start
        *************************************-->
    <section id="hb-blog" class="hb-blog hb-sectionspace hb-haslayout">
        <div class="container">
            <div class="row">
                <div class="hb-post-area hb-bloggridfullwidth">
                    @foreach($categories as $category)
                        <a href="{{ url($category->slug) }}">
                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                                <article class="hb-post text-center">
                                    <figure class="hb-postimage hb-postimage-art" style="height: auto;">
                                        <img src="/storage/{{ $category->icon }}"
                                             alt="image description">
                                    </figure>
                                    <div class="hb-postcontent">
                                        <div class="hb-posttitle">
                                            <h3>
                                                {{ $category->name }}
                                            </h3>
                                        </div>
                                        <div class="hb-postmeta">
                                            <span>{{ $category->short_description }}</span>
                                        </div>
                                    </div>
                                </article>
                            </div>
                        </a>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
    <!--************************************
                Blog End
    *************************************-->
@endsection
