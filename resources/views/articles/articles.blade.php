@extends('layouts.app', [
    'title' => "$category->name - Новости"
])

@section('breadcrumbs')
    <div id="hb-innerbanner" class="hb-innerbanner hb-haslayout">
        <div class="containers">
            <div class="hb-innerbanner-area">
                <div class="hb-bannarheading">
                    <h1>{{ $category->name }}</h1>
                </div>
                <ul class="hb-breadcrumb">
                    <li><a href="{{ route('home') }}">Главная</a></li>
                    <li><a href="{{ route('article.categories') }}">Новости и события</a></li>
                    <li>{{ $category->name }}</li>
                </ul>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <!--************************************
                    Blog Start
        *************************************-->
    <section id="hb-blog" class="hb-blog hb-sectionspace hb-haslayout">
        <div class="container">
            <div class="row">
                <div class="hb-post-area hb-bloggridfullwidth">
                    @if(!$articles->count())
                        <section id="hb-error" class="hb-error hb-sectionspace hb-haslayout">
                            <div class="container">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="hb-error-area">
                                            <h2 class="error-code">😒</h2>
                                            <div class="hb-errorcontent">
                                                <h3 class="title">В выбранном разделе пока нет новостей</h3>
                                                <p>Попробуйте вернуться сюда позже</p>
                                            </div>
                                            <a href="{{ route('article.categories') }}" class="hb-btn">Вернуться к
                                                разделам</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    @endif
                    @foreach($articles as $article)
                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                            <article class="hb-post">

                                    <a href="{{ url('/articles/'.$article->articleCategory->slug.'/'.$article->slug) }}">
	                                <figure class="hb-postimage">
        	                                <img src="{{ \Voyager::image($article->image) }}" alt="{{ $article->title }}" />
					</figure>
                                    </a>



                                <div class="hb-postcontent">
                                    <a href="{{ route('article.articles', ['category' => $article->articleCategory->slug]) }}"
                                       class="hb-postcategory">
                                        {{ $article->articleCategory->name }}
                                    </a>
                                    <div class="hb-posttitle">
                                        <h3>
                                            <a href="{{ url('/articles/'.$article->articleCategory->slug.'/'.$article->slug) }}">{{ $article->title }}</a>
                                        </h3>
                                    </div>
                                    <a href="{{ url('/articles/'.$article->articleCategory->slug.'/'.$article->slug) }}">
                                    <div class="hb-post-area">
                                        <span>
                                            {{ $article->small_desc }}
                                        </span>
                                    </div></a>
<!--
                                    <div class="hb-postmeta" style="margin-top: 25px;">
                                        <span>{{ $article->created_at->diffForHumans() }}</span>
                                    </div>
-->
                                </div>
                            </article>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
    <!--************************************
                Blog End
    *************************************-->
@endsection
