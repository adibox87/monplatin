<html lang="ru">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <style>
        *{ font-family: DejaVu Sans !important;}
    </style>
    <title>Заказ №{{ $order->id }}</title>
</head>
<body>
<div style="width: 100%; max-width: 960px; margin: auto">
    <table width="100%">
        <tr style="border-bottom: 1px solid #000000">
            <td><h2>Заказ</h2></td>
            <td style="text-align: right"><h3>Заказ №{{ $order->id }}</h3></td>
        </tr>
        <tr>
            <td style="padding-bottom: 16px;">
                <strong>Продавец:</strong><br>
                MONPLATIN<br>
                пр-т Независимости 52<br>
                пом. 3<br>
                Минск, Минская область, BY
            </td>
            <td style="text-align: right; padding-bottom: 16px;">
                <strong>Покупатель:</strong><br>
                {{ $order->address['name'] }} {{ $order->address['surname'] }}<br>
                @if(isset($order->address['street']))
                {{ $order->address['street'] }}<br>
                {{ $order->address['street_extra'] }}<br>
                {{ $order->address['city'] }}, {{ $order->address['state'] }}, {{ $order->address['country_code'] }}<br>
                {{ $order->address['post_code'] }}
                @endif
            </td>
        </tr>
        <tr>
            <td style="padding-bottom: 16px;">
            </td>
            <td style="text-align: right;padding-bottom: 16px;">
                {{ $order->address['email'] }}<br>
                {{ $order->address['phone'] }}
            </td>
        </tr>
        <tr>
            <td>
                <strong>Оплата:</strong><br>
                Оплата при получении
            </td>
            <td style="text-align: right">
                <strong>Дата заказа:</strong><br>
                {{ optional($order->created_at)->formatLocalized('%d %B %Y в %H:%M') }}<br><br>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <h3>Детали заказа</h3>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <table width="100%" cellpadding="0" cellspacing="0" border="1">
                    <thead>
                    <tr style="background-color: #eee">
                        <th style="text-align: left; padding: 5px 10px;">Товар</th>
                        <th style="text-align: center; padding: 5px 10px;">Цена</th>
                        <th style="text-align: center; padding: 5px 10px;">Кол-во</th>
                        <th style="text-align: right; padding: 5px 10px;">Сумма</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($order->cart as $cartItem)
                        <tr>
                            <td style="text-align: left; padding: 5px 10px;">
                                {{ $cartItem['name'] }}
                            </td>
                            <td style="text-align: center; padding: 5px 10px;">{{ $cartItem['price'] }}</td>
                            <td style="text-align: center; padding: 5px 10px;">
                                {{ $cartItem['quantity'] }}
                            </td>
                            <td style="text-align: right; padding: 5px 10px;">{{ $cartItem['quantity'] * $cartItem['price'] }}</td>
                        </tr>
                    @endforeach
                    @foreach($order->conditions as $title => $condition)
                        <tr>
                            <td style="text-align: left; padding: 5px 10px;">
                                {{ $title }}
                            </td>
                            <td style="text-align: center; padding: 5px 10px;">{{ round($condition['parsedRawValue'], 2) }}</td>
                            <td style="text-align: center; padding: 5px 10px;">&mdash;</td>
                            <td style="text-align: right; padding: 5px 10px;">{{ round($condition['parsedRawValue'], 2) }}</td>
                        </tr>
                    @endforeach
                    <tr>
                        <td colspan="2"></td>
                        <td style="text-align: center; padding: 5px 10px;"><strong>Всего</strong></td>
                        <td style="text-align: right; padding: 5px 10px;">{{ $order->total }}</td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </table>
</div>
</body>
</html>
