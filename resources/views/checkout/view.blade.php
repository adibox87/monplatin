@extends('layouts.app', [
    'title' => 'Заказ №' . $order->id
])

@section('content')
    <section>
        <div class="container">
            <div class="row order">
                <div class="col-12 col-md-8">
                    <div class="product-table">
                        <table>
                            <thead>
                            <tr>
                                <th class="table-header">Товар</th>
                                <th class="table-header">Цена</th>
                                <th class="table-header">Количество</th>
                                <th class="table-header">Сумма</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($cart = $order->cart as $cartItem)
                                <tr>
                                    <td class="table-item">
                                        {{ $cartItem['name'] }}
                                    </td>
                                    <td class="table-item">{{ $cartItem['price'] }}</td>
                                    <td class="table-item">
                                        {{ $cartItem['quantity'] }}
                                    </td>
                                    <td class="table-item">{{ $cartItem['quantity'] * $cartItem['price'] }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <span>
                            <i class="fas fa-file-pdf"></i>
                            <a href="{{ URL::signedRoute('checkout.pdf', $order) }}" target="_blank" class="btn btn-link">Скачать форму заказа</a>
                        </span>
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <div class="order-information">
                        <div class="cart">
                            <div class="order-subtotal">
                                <h3>Спасибо, Ваш заказ был получен</h3>
                                <p class="order-subtitle">Итого</p>
                                <strong>{{ $order->subtotal }}</strong>
                            </div>
                            <div class="order-delivery">
                                <div class="delivery-option">
                                    <p class="order-subtitle">Дополнительно</p>
                                    <ul>
                                        @foreach($order->conditions as $title => $condition)
                                            <li>
                                            {{ $title }}: {{ round($condition['parsedRawValue'], 2) }} руб.
                                            </li>
                                        @endforeach
                                        <li>
                                            Метод оплаты: Оплата при доставке
                                        </li>
                                        <li>
                                            Дата: {{ optional($order->created_at)->formatLocalized('%d %B %Y в %H:%M') }}
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="order-total">
                                <p class="order-subtitle">Всего</p>
                                <strong id="total-price">{{ $order->total }}</strong>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
