@extends('layouts.app', [
    'title' => 'Оформление заказа'
])
@section('breadcrumbs')
    <div id="hb-innerbanner" class="hb-innerbanner hb-haslayout">
        <div class="containers">
            <div class="hb-innerbanner-area">
                <div class="hb-bannarheading">
                    <h1>Магазин</h1>
                </div>
                <ul class="hb-breadcrumb">
                    <li><a href="{{ route('home') }}">Главная</a></li>
                    <li>Магазин</li>
                </ul>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <section id="checkout">
        <div class="container pt5">
            <div class="delivery-information">
                <div class="mb2">
                    <h2 class="mb2"><strong>Условия доставки</strong></h2>
                    <p class="mb2 attention"><i class="fas fa-lightbulb mr1"></i>Все акции действуют только при покупке товаров в фирменном магазине. На интернет-заказы акции
                        не распространяются!
                    </p>
                </div>
                <div class="hb-faq-area">
                    <div id="hb-accordion" class="hb-accordian">
                        <div class="hb-panel">
                            <h4><strong>Условия доставки курьером по г. Минску:</strong></h4>
                            <div class="hb-panelcontent">
                                <div class="hb-description">
                                    <ul class="mb2">
                                        <li>Самовывоз - бесплатно - 2 пункта доступен от 1 рубля</li>
                                        <li>При оформлении заказа от 30 рублей до 60 рублей – доставка 6 рублей по г. Минску.</li>
                                        <li>При оформлении заказа от 60 рублей – доставка по городу Минску бесплатная.</li>
{{--                                        <li>При оформлении заказа от 30 до 100 рублей – доставка почтой по р.Беларусь - платно.</li>--}}
{{--                                        <li>При оформлении заказа от 100 рублей – доставка почтой бесплатная.</li>--}}
                                    </ul>
                                    <h5><strong>Время доставки:</strong></h5>
                                    <ul class="mb2">
                                        <li><strong>Будние дни ПН-ПТ:</strong> с 11:00 до 20:00</li>
                                        <li><strong>Суббота:</strong> с 11:00 до 18:00</li>
                                        <li><strong>Воскресенье и праздничные дни:</strong> доставка не осуществляется.</li>
                                    </ul>
                                    <p class="attention"><i class="fas fa-clock mr1"></i>Окончательное время доставки согласовывает консультант-продавец, после оформления заказа.</p>
                                </div>
                            </div>
                        </div>
                        <div class="hb-panel">
                            <h4><strong>Условия доставки по РБ:</strong></h4>
                            <div class="hb-panelcontent">
                                <div class="hb-description">
                                    <p>По Республике Беларусь доставка осуществляется РУП «Белпочта» при оформлении заказа от 60 руб.
                                        наложенным платежом. Срок доставки до Вашего населенного пункта, указанного в заказе 3-4 дня с
                                        момента подтверждения заказа и передачи отправления РУП «Белпочта».</p>
                                    <h5><strong>Стоимость доставки:</strong></h5>
                                    <ul>
                                        <li><strong>Сумма заказа от 60 до 100 руб</strong>.- доставка за счет покупателя (стоимость доставки
                                            рассчитывается по актуальным тарифам РУП «Белпочта» http://www.belpost.by/)</li>
                                        <li><strong>Сумма заказа более 100 руб</strong>.- бесплатная доставка (за счет отправителя).</li>
                                    </ul>
                                    <p>Доставка осуществляется по адресу, указанному при оформлении заказа. Продавец не несет
                                        ответственности за задержку в доставке заказа Покупателю в случае неполных и/или недостоверных
                                        данных, указанных Покупателем при оформлении заказа.</p>
                                    <p>Срок хранения отправления в почтовом отделении составляет 10 дней.</p>
                                    <p>Повторная доставка осуществляется за счет покупателя, вне зависимости от суммы заказа.</p>
                                    <p class="attention"><i class="fas fa-mobile-alt mr1"></i><b>Телефон отдела почтовых отправлений для справки: +375177554722</b></p>
                                </div>
                            </div>
                        </div>
                        <div class="hb-panel">
                            <h4><strong>Пункт самовывоза:</strong></h4>
                            <div class="hb-panelcontent">
                                <div class="hb-description">
                                    <div>Магазин Мон Платин Центр – <b>г. Минск, пр-т Независимости, 52</b></div>
                                    <ul>
                                        <li><strong>ПН-СБ</strong>: с 10:00 до 21:00</li>
                                        <li><strong>ВС:</strong>&nbsp;с 10:00 до 18:00</li>
                                    </ul>
                                    <div>Магазин Мон Платин Центр – <b>г. Минск, ул. Романовская Слобода, 9</b></div>
                                    <ul>
                                        <li><strong>ПН-СБ</strong>: с 10:00 до 21:00</li>
                                        <li><strong>ВС:</strong>&nbsp;с 10:00 до 18:00</li>
                                    </ul>
                                    <p>Доставка товаров осуществляется исключительно в пределах территории Республики Беларусь по
                                        адресу, указанному Покупателем во время оформления заказа.
                                        Продавец не несет ответственности за невозможность доставить товар и связаться с покупателем в
                                        случае неполных или недостоверных данных, указанных покупателем при оформлении заказа.</p>
                                    <p class="attention"><i class="fas fa-mobile-alt mr1"></i><b>Дополнительную информацию по условиям доставки можно получить по телефонам:</b>
                                        +375(17) 318-80-99, 8(033) 357-01-01 (мтс), 8(029) 357-01-01 (вел)</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <h3 class="pt5 mb2 text-center" id="form-start">Оформление заказа</h3>
            @guest('customer')
            <p class="text-center">Уже покупали у нас? <a href="{{ route('customer.login') }}"><strong>Нажмите здесь, чтобы войти</strong></a></p>
            @endguest
            @if($errors->any())
                <div class="row">
                    <div class="col-12">
                        <div class="alert alert-danger" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>

                            @foreach($errors->all() as $error)
                                {{ $error }}<br/>
                            @endforeach
                        </div>
                    </div>
                </div>
            @endif
            <div class="order">
                <form id="checkout-form" method="post" style="width: 100%;" action="{{ route('checkout.post') }}">
                <div class="col-12 col-md-8">
                    <div class="order-form">

                            @csrf

                            <div class="row mt-15 mb-15 none"  id="checkout-error">
                                <div class="col-md-12 text-danger text-sm-left">Проверьте корректность введенных данных</div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-6">
                                    <div class="form-group">
                                        <label for="name">Имя <strong>*</strong></label>
                                        <input
                                            type="text"
                                            class="form-control"
                                            id="name"
                                            name="name"
                                            value="{{ old('name') ?? optional(auth('customer')->user())->name }}"
                                            autocomplete="given-name"
                                            oninvalid="this.setCustomValidity('Введите имя получателя')"
                                            placeholder="Имя получателя">
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-6">
                                    <div class="form-group">
                                        <label for="surname">Фамилия <strong>*</strong></label>
                                        <input
                                            type="text"
                                            class="form-control"
                                            id="surname"
                                            name="surname"
                                            value="{{ old('surname') ?? optional(auth('customer')->user())->surname }}"
                                            autocomplete="family-name"
                                            oninvalid="this.setCustomValidity('Фамилия получателя')"
                                            placeholder="Фамилия получателя">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-6">
                                    <div class="form-group">
                                        <label for="phone">Телефон (+375-XX-XXX-XX-XX) <strong>*</strong></label>
                                        <input
                                            class="form-control"
                                            id="phone"
                                            name="phone"
                                            value="{{ old('phone') ?? optional(auth('customer')->user())->phone }}"
                                            type="tel"
                                            placeholder="+375-XX-XXX-XX-XX">
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-6">
                                    <div class="form-group">
                                        <label for="email">Email <strong>*</strong></label>
                                        <input
                                            type="text"
                                            class="form-control"
                                            id="email"
                                            name="email"
                                            value="{{ old('email') ?? optional(auth('customer')->user())->email }}"
                                            oninvalid="this.setCustomValidity('Введите корректный email')"
                                            placeholder="Email">
                                    </div>
                                </div>
                            </div>
                            <div data-delivery-subform>
                            </div>
{{--                            <button type="submit" id="checkout-submit">123</button>--}}
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    @include('cart._order-info', ['checkout' =>  true])
                </div>
                </form>
            </div>
        </div>
    </section>
    <div data-options-container>
        <div data-option="cherrypick" class="none">
            @include('customer.cherrypick', [ 'salons' =>  $salons ])
        </div>
        <div data-option="delivery" class="none">
            @include('customer.address', [ 'address' =>  optional(auth('customer')->user())->getPrimaryAddress() ?? null ])
        </div>
    </div>
@endsection

<style>
    .none {
        display: none;
    }

    .mb2 {
        margin-bottom: 2rem !important;
    }

    .mr1 {
        margin-right: 1rem !important;
    }

    .pt5 {
        padding-top: 5rem !important;
    }

    .pb5 {
        padding-bottom: 5rem !important;
    }

    .delivery-information ul {
        padding-left: 18px;
    }

    .delivery-information li:not(:last-child) {
        border-bottom: 1px solid gray;
    }

    .delivery-information li {
        padding: 1rem;
    }

    .attention {
        background-color: #EBECED;
        padding: 2rem;
    }
</style>
