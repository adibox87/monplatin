<div class="hb-widget sort">
    <div class="hb-widgettitle">
        <h3>Фильтр</h3>
    </div>
{{--    <form  action="{{ route('catalog.index') }}">--}}
{{--        @foreach($brands as $brand)--}}
{{--            <input type='checkbox' name='brands[]' value='{{ $brand->id }}'>  {{ $brand->title }}<br>--}}
{{--        @endforeach--}}
{{--            <button type="submit" class="hb-btn">Применить</button>--}}
{{--    </form>--}}


    <form id="target">
        @foreach($brands as $brand)
            <input type='checkbox' class="lines" id="{{ $brand->id }}" name='brands[]' value='{{ $brand->id }}'>  {{ $brand->title }}<br>
        @endforeach


        <button id="filter-btn" class="hb-btn mt-10">Применить</button>
        <div class="mt-5 mb-5" style="cursor: pointer; text-transform: uppercase;" id="clear-filter"><strong><small>Сбросить фильтры</small></strong></div>

    </form>

</div>
