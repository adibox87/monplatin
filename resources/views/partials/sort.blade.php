<div class="filters">
    <form action="{{ route('catalog.index') }}">
        <div class="row">
            <div class="col-md-4">
                По популярности
                <select type="submit" name="sort" class="form-control-sm form-control">
                    <option name="" value="">По умолчанию</option>
                    <option name="inc_view" value="inc_view">По возрастанию</option>
                    <option name="dec_view" value="dec_view">По убыванию</option>
                </select>
            </div>
            <div class="col-md-4">
                По цене
                <select name="sort" class="form-control-sm form-control">
                    <option name="inc_price" selected value="inc_price">По возрастанию</option>
                    <option name="dec_price" value="dec_price">По убыванию</option>
                </select>
            </div>
            <div class="col-md-4" style="padding: 20px;">
                <button type="submit" class="hb-btn">Сортировать</button>
            </div>
        </div>
    </form>
</div>
