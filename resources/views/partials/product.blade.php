<article class="hb-post product" itemprop="itemListElement" onclick="window.location.href = '/{{$product->getUrl()}}';">
    <meta itemprop="position" value="" />
    <link itemprop="url" href="/{{$product->getUrl()}}" />
    @if($product->new)
        <span class="new-product">Новинка</span>
    @endif
    <figure class="hb-product-image img-hover-zoom img-hover-zoom--zoom-n-rotate">
        <a href="/{{ $product->getUrl() }}">
            @if($product->image)
                <img itemprop="image" src="/storage/{{$product->image}}" alt="{{ $product->title }}">
            @else

            @endif
        </a>
    </figure>
        @if($product->sale_price)
            <span class="product-price"><d style="text-decoration: line-through;font-size: 18px;color: #d8d8d8;padding: 20px;">{{ $product->price }} руб.</d>{{ $product->sale_price }} руб.</span>
        @else
            <span class="product-price">{{ $product->price }} руб.</span>
        @endif
    <div class="hb-postcontent">
        <div class="hb-posttitle product-title">
            <p itemprop="name"><span class="product-name">{{ $product->title }}</span></p>
        </div>
        <a href="{{route('cart.add', $product->id)}}" class="hb-btn" style="width: 100%;">Добавить в корзину</a>
    </div>
</article>
