<div class="hb-widget hb-categories">
    <div class="hb-widgettitle">
        <h3>Поиск</h3>
    </div>
    <div class="search-catalog">
        <form action="{{ route('search.index') }}">
            <input aria-label="Поиск" type="text" placeholder="Поиск" name="search" class="hb-emailarea hb-formtheme form-control">
        </form>
    </div>
</div>

<div class="hb-widget hb-categories">
    <div class="hb-widgettitle">
        <h3>Категории</h3>
    </div>
    <ul>
        <li><a href="{{ route('catalog.index') }}">Все товары</a></li>
        @foreach($categories as $category)
            <li><a href="/shop/{{$category->slug}}">{{ $category->name }} ({{ $category->products->count() }})</a></li>
        @endforeach
        <li><a href="{{ route('catalog.new') }}">Новинки ({{ \App\Models\Product\Product::sort()->filter()->whereHas('categories')->where('new', true)->count() }})</a></li>
    </ul>
</div>

@include('partials.filter')

<div class="hb-widget hb-recentnews">
    <div class="hb-widgettitle">
        <h3>Акции</h3>
    </div>
    <ul>
        @foreach($stocks as $stock)
            <li>
                <h4><a href="/articles/akcii-magazina/{{ $stock->slug }}">{{ $stock->title }}</a></h4>
                <div class="hb-postmeta">
                    <span>{{ $stock->small_desc }}</span>
                    <span style="padding: 20px">{{ $stock->created_at->diffForHumans() }}</span>
                </div>
            </li>
        @endforeach
    </ul>
</div>
