<div class="row mb-20">
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
        <article class="hb-post text-center mb-10">
            <a href="{{ route('catalog.new') }}"class="hb-postcontent">
                <div class="hb-posttitle">
                    <h3>Новинки <i class="ti-star"></i></h3>
                </div>
                <div class="hb-postmeta">
                    <span>В данном разделе находятся все новые товары <b>"Mon Platin"</b></span>
                </div>
            </a>
        </article>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
        <article class="hb-post text-center mb-10 position-relative">
            <a href="{{ route('catalog.popular') }}" class="hb-postcontent">
                <div class="hb-posttitle">
                    <h3>Популярные <i class="ti-star"></i></h3>
                </div>
                <div class="hb-postmeta">
                    <span>В данном разделе находятся все популярные товары <b>"Mon Platin"</b></span>
                </div>
            </a>
        </article>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
        <article class="hb-post text-center mb-10">
            <a href="{{ route('catalog.sale') }}" class="hb-postcontent">
                <div class="hb-posttitle">
                    <h3>Акции <i class="ti-star"></i></h3>
                </div>
                <div class="hb-postmeta">
                    <span>В данном разделе находятся все акционные товары <b>"Mon Platin"</b></span>
                </div>
            </a>
        </article>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <p class="text-center text-muted">
            Парфюмерно-косметические товары возврату и обмену не подлежат согласно Постановлению Совета Министров Республики Беларусь № 778 от 14.06.2002 года
        </p>
    </div>
</div>
