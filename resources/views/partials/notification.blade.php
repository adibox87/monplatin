@if($notifications)
    <div>
        <script>
            document.addEventListener("DOMContentLoaded", function () {
                if (localStorage.getItem("banners") != {{$notifications->id}}) {
                    console.log('first show')
                    openPopup({{$notifications->id}})
                    localStorage.setItem("banners", {{$notifications->id}});
                }
            });

            function openPopup($id) {
                document.getElementById("banner-" + $id).style.display = "block";
                document.getElementById("overlay").style.display = "block";
            }

            function closePopup($id) {
                document.getElementById("banner-" + $id).style.display = "none";
                document.getElementById("overlay").style.display = "none";

            }
        </script>

        <div id="overlay"></div>
        <div class="banner" id="banner-{{$notifications->id}}">
            <a href="{{$notifications->url}}">
                <img src="{{ Voyager::image( $notifications->image ) }}" alt="">
                <div onclick="closePopup({{$notifications->id}})" class="closebanner">ЗАКРЫТЬ</div>

            </a>

        </div>
    </div>
@endif
