@extends('layouts.simple', [
    'bodyClass' => @$bodyClass,
    'title' => @$title ? $title . ' - ' . setting('site.title', config('app.name')) : setting('site.title', config('app.name')),
    'description' => @$description ? $description : setting('site.description')
])

@push('html-head-bottom')
    @notifyCss
<!--
    <script>
        (function (d, w, c) {
            w.ChatraID = '{{ setting('site.chatra_id', 'dmorqwDTvrb8z62Hd') }}';
            var s = d.createElement('script');
            w[c] = w[c] || function () {
                (w[c].q = w[c].q || []).push(arguments);
            };
            s.async = true;
            s.src = 'https://call.chatra.io/chatra.js';
            if (d.head) d.head.appendChild(s);
        })(document, window, 'Chatra');
    </script>
-->
@endpush

@push('html-body-top')
    @include('notify::messages')
@endpush

@section('navbar')
    @widget('navbar-widget')
@endsection


@section('app-content')
    @yield('breadcrumbs')


    @include('partials.notification')

    <div id="hb-main" class="hb-main hb-haslayout">
        @yield('content')
    </div>
@endsection

@push('html-body-bottom')
<!--    <div id="chatra-wrapper"></div>-->
<!--
    <script>
        window.ChatraSetup = {
            colors: {
                buttonText: '#f0f0f0', /* цвет текста кнопки чата */
                buttonBg: '#a42c59',    /* цвет фона кнопки чата */
                mode: 'frame',
                injectTo: 'chatra-wrapper'
            }
        };
    </script>
-->

    @include('app._modal-contacts')
    <script type="text/javascript">
        function form_submitContact() {
            document.getElementById("contactFormContact").submit();
        }
    </script>

    @notifyJs
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAutoT4vldCwtVFb3oDjrbZYY-pYQeebls"></script>
@endpush
