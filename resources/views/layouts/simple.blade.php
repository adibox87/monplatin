@extends('layouts._html5', [
    'title' => @$title,
])

@push('html-head-bottom')
    <!-- =====  CSRF Token  ===== -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- =====  Fonts  ===== -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">

    <!-- =====  CSS  ===== -->
    <link rel="stylesheet" type="text/css" href="{{ url('css/bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('css/icomoon.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ url('css/icofont.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ url('css/plugins.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ url('css/style.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ url('css/color.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ url('css/responsive.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ url('css/modal.css') }}"/>
    <link href="https://fonts.googleapis.com/css?family=Rubik&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Montserrat:wght@900&family=Nunito+Sans:wght@900&amp;text=D.1&amp;display=swap">
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('favicon.png') }}">

    @if(setting('site.google_analytics_tracking_id'))
        <!-- Google Analytics -->
        <script>
            window.ga=window.ga||function(){(ga.q=ga.q||[]).push(arguments)};ga.l=+new Date;
            ga('create', '{{ setting('site.google_analytics_tracking_id') }}', 'auto');
            ga('send', 'pageview');
        </script>
        <script async src='https://www.google-analytics.com/analytics.js'></script>
        <!-- End Google Analytics -->
    @endif
@endpush

@push('html-body-top')

@endpush

@push('html-body-bottom')
    <script src="{{ url('js/vendor/modernizr-2.8.3-respond-1.4.2.min.js') }}"></script>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="{{ url('js/vendor/jquery-library.js') }}"></script>
    <script src="{{ url('js/vendor/bootstrap.min.js') }}"></script>
    <script src="{{ url('js/owl.carousel.min.js') }}"></script>
    <script src="{{ url('js/isotope.pkgd.min.js') }}"></script>
    <script src="{{ url('js/isotop.js') }}"></script>
    <script src="{{ url('js/countTo.js') }}"></script>
    <script src="{{ url('js/appear.js') }}"></script>
    <script src="{{ url('js/main.js') }}"></script>
    <script src="{{ url('js/popper.min.js') }}"></script>
    <script src="{{ url('js/modal.js') }}"></script>
    <script src="{{ url('js/validator.js') }}"></script>
@endpush

@section('app-header')
    @yield('navbar')
    @include('app._search-popup')
@endsection

@section('app-footer')
    @widget('footer-widget')
@endsection

@section('html-body')
    <div id="app">
        <!-- =====  HEADER START  ===== -->
        <header id="header">
            @yield('app-header')
        </header>
        <!-- =====  HEADER END  ===== -->
        <!-- =====  CONTAINER START  ===== -->
        <main>
            @yield('app-content')
        </main>
        <!-- =====  CONTAINER END  ===== -->
        <!-- =====  FOOTER START  ===== -->
        <footer class="footer pt_60">
            @yield('app-footer')
        </footer>
        <!-- =====  FOOTER END  ===== -->
    </div>
@endsection
