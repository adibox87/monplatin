@extends('layouts.app', [
    'title' => 'Магазин',
    'description' => $product->short_description,
])

@section('breadcrumbs')
    <div id="hb-innerbanner" class="hb-innerbanner hb-haslayout">
        <div class="containers">
            <div class="hb-innerbanner-area">
                <ul class="hb-breadcrumb">
                    <li><a href="{{ route('home') }}">Главная</a></li>
                    <li><a href="{{ route('catalog.index') }}">Магазин</a></li>
                    <li><a href="{{ route('catalog.index') }}/{{ $category->slug }}">{{ $category->name }}</a></li>
                    <li>{{ $product->title }}</li>
                </ul>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <section id="hb-blog" class="hb-blog hb-sectionspace hb-haslayout">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12">
                    <div class="hb-post-area hb-blogdetails">
                        <article class="hb-post">
                            @if($product->new)
                                <span class="new-product">Новинка</span>
                            @endif
                            <figure>
                                <a href="#" data-image-id="" data-toggle="modal" data-title=""
                                   data-image="/storage/{{ $product->image }}"
                                   data-target="#image-gallery">
                                    <img src="/storage/{{ $product->image }}" alt="image description"
                                         style="width: 300px">
                                </a>
                            </figure>
                            <h2>{{ $product->title }}</h2>
                            <ul class="list-unstyled hb-treatmentlist" style="padding: 15px;line-height: 35px;">
                                        <span class="categories">Категории:
                                            @foreach($product->categories as $category)
                                                <b><a href="/shop/{{ $category->slug }}">{{ $category->name }}</a></b>
                                            @endforeach
                                        </span>
                                </br>
                                @if($product->vendor_code)
                                    <span>Артикул: <b>{{ $product->vendor_code }}</b></span>
                                @endif
                                <br>
                                <h2>{{ $product->price }} р.</h2>
                                <form action="{{ route('cart.add', $product->id) }}" method="get">
                                    <div class="input-group number-spinner spinner mb1">
                                        <span class="input-group-btn">
                                            <button
                                                type="button"
                                                class="btn btn-default"
                                                data-dir="dwn">-</button>
                                        </span>
                                            <input
                                                type="text"
                                                readonly
                                                class="form-control text-center"
                                                name="quantity"
                                                value="1"
                                            />
                                            <span class="input-group-btn">
                                        <button
                                            type="button"
                                            class="btn btn-default"
                                            data-dir="up">+</button>
                                        </span>
                                    </div>
                                    <button class="hb-btn" type="submit">Добавить в корзину</button>
                                </form>
                                <hr>
                                <h3>Описание</h3>
                                {!! $product->description !!}
                                
                                <br>
                                    <span><b>Производитель: </b>"A-Meshi Industries" 8 Shimon Haborseki St., Industrial Area, Bat Yam, Israel, Адреса филиалов: 1) 4 Pinkas St., Rishon le Zion, Israel; 2) 8 Shimon Haborseki St., Industrial Area, Bat Yam, Israel</span>
                                
                            </ul>
                        </article>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-xs-12 col-sm-12">
                    <h3 style="padding: 20px;">Похожие товары</h3>
                    @foreach($relatedProducts as $rProduct)
                        @if(!$rProduct->is($product))
                            <div class="col-xs-12 col-sm-4 col-lg-3 mb-20">
                                @include('partials.product', ['product' => $rProduct])
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
    </section>
@endsection
