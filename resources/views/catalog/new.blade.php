@extends('layouts.app', [
    'title' => 'Магазин | Новинки'
])

@section('breadcrumbs')
    <div id="hb-innerbanner" class="hb-innerbanner hb-haslayout">
        <div class="containers">
            <div class="hb-innerbanner-area">
                <div class="hb-bannarheading">
                    <h1>Новинки</h1>
                </div>
                <ul class="hb-breadcrumb">
                    <li><a href="{{ route('home') }}">Главная</a></li>
                    <li><a href="{{ route('catalog.index') }}">Магазин</a></li>
                    <li>Новинки</li>
                </ul>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <section id="hb-blog" class="hb-blog hb-sectionspace hb-haslayout">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-lg-3 col-sm-4">
                    @include('partials.categories_tree')
                </div>
                <div class="col-xs-12 col-lg-9 col-sm-8">
                    @include('partials.catalog_help')
                    @include('partials.sort')
                    <div class="row">
                        @foreach($products as $product)
                            <div class="col-xs-12 col-sm-4 col-lg-4 mb-20">
                                @include('partials.product')
                            </div>
                        @endforeach
                    </div>
                    {!! $products->links() !!}
                </div>
            </div>
        </div>
    </section>
@endsection
