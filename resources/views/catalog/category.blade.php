@extends('layouts.app', [
    'title' => $category->name
])

@section('breadcrumbs')
    <div id="hb-innerbanner" class="hb-innerbanner hb-haslayout">
        <div class="containers">
            <div class="hb-innerbanner-area">
                <div class="hb-bannarheading">
                    <h1>{{ $category->name }}</h1>
                </div>
                <ul class="hb-breadcrumb">
                    <li><a href="{{ route('home') }}">Главная</a></li>
                    <li><a href="{{ route('catalog.index') }}">Магазин</a></li>
                    <li>{{ $category->name }}</li>
                </ul>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <section id="hb-blog" class="hb-blog hb-sectionspace hb-haslayout">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-lg-3 col-sm-4">
                    @include('partials.categories_tree')
                </div>
                <div class="col-xs-12 col-lg-9 col-sm-8">
                    <div class="row">

                        @if(count($products) > 0)


                                @foreach($products as $product)
                                    <div class="col-xs-12 col-sm-4 col-lg-4 mb-20 " data-line="{{ $product->product_line_id }}" >
                                        @include('partials.product')
                                    </div>
                                @endforeach

                        @else
                            <h3 class="text-center text-primary"><strong>Товары не найдены</strong></h3>
                        @endif
                    </div>
                    {!! $products->links() !!}
                </div>
            </div>
        </div>
    </section>
@endsection
