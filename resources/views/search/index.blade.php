<title>Магазин</title>

@extends('layouts.app')

@section('breadcrumbs')
    <div id="hb-innerbanner" class="hb-innerbanner hb-haslayout">
        <div class="containers">
            <div class="hb-innerbanner-area">
                <div class="hb-bannarheading">
                    <h1>Результаты поиска</h1>
                </div>
                <ul class="hb-breadcrumb">
                    <li><a href="{{ route('home') }}">Главная</a></li>
                    <li>Поиск</li>
                </ul>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <section id="hb-blog" class="hb-blog hb-sectionspace hb-haslayout">
        <div class="container">
            <div class="row">
                <form action="{{ route('search.index') }}">
                    <div class="col-xs-12 col-sm-12 col-12 text-center">
                        <input type="text" placeholder="Поиск" name="search" class="search-form">
                        <button type="submit" class="hb-btn hidden-xs" style="height: 40px;">Искать</button>
                    </div>
                </form>
                <hr>
                <br><br>
                @if($products->count() < 1)
                    <div class="col-xs-12">
                        <div class="hb-error-area">
                            <div class="hb-errorcontent">
                                <h3 class="title">Упс...</h3>
                                <p>К сожалению по Вашему запросу товаров не найдено</p>
                            </div>
                            <a href="{{ route('home') }}" class="hb-btn">Вернуться на главную</a>
                        </div>
                    </div>
                @else
                    <div class="col-xs-12">
                        <div class="hb-error-area">
                            <div class="hb-errorcontent">
                                <h3 class="title">Найдено товаров: {{ $count }}</h3>
                            </div>
                        </div>
                    </div>
                @endif
                @foreach($products as $product)
                    <div class="col-xs-12 col-sm-3 col-lg-3 mb-20">
                        @include('partials.product')
                    </div>
                @endforeach
            </div>
            {!! $products->withQueryString()->links() !!}
        </div>
    </section>
@endsection
