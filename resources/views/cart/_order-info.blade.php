<div class="order-information">
    <div class="cart">

        @if(!isset($checkout))
        <form>
        @endif
            <div class="order-subtotal">
                <h3>Сумма заказов</h3>
                <p class="order-subtitle">Итого</p>
                <strong>{{ Cart::getSubTotal() }}</strong>
            </div>
            <div class="order-delivery">
                <div class="delivery-option">
                    <p class="order-subtitle">Доставка</p>
                    <ul data-select-delivery-type>
                        @foreach($deliveryOptions as $option)
                            <li>
                                <label for="deliveryOption{{$option->id}}">
                                    <input
                                        required
                                        type="radio"
                                        name="shipping_method"
                                        id="deliveryOption{{ $option->id }}"
                                        value="{{ $option->id }}"
                                        data-id="{{$option->variant}}"
                                        data-price="{{ $option->price }}"
                                    />
                                    @if($option->price == 0)
                                        {{ $option->title }}
                                    @else
                                        {{ $option->title }}: {{ round($option->price, 2) }} руб.
                                    @endif
                                </label>
                            </li>
                        @endforeach
                    </ul>
                </div>
                <div class="delivery-time" style="display: none;">
                    <p class="order-subtitle">Желаемое время доставки</p>
                    <ul>
                        <li>
                            <label for="delivery-11-14">
                                <input
                                    type="radio"
                                    name="delivery_time"
                                    id="delivery-11-14"
                                    value="11:00 - 14:00"
                                />
                                11:00 - 14:00
                            </label>
                        </li>
                        <li>
                            <label for="delivery-14-17">
                                <input
                                    type="radio"
                                    name="delivery_time"
                                    id="delivery-14-17"
                                    value="14:00 - 17:00"
                                />
                                14:00 - 17:00
                            </label>
                        </li>
                        <li>
                            <label for="delivery-17-20">
                                <input
                                    type="radio"
                                    name="delivery_time"
                                    id="delivery-17-20"
                                    value="17:00 - 20:00"
                                />
                                17:00 - 20:00
                            </label>
                        </li>
                    </ul>
                </div>
            </div>
            <div data-cherrypick-subform class="mt-15">
            </div>
            <div class="order-total">
                <p class="order-subtitle">Всего</p>
                <strong id="total-price">{{ Cart::getTotal() }}</strong>
            </div>

            @if(isset($checkout))
            <button type="submit" class="hb-btn hb-btn-lg">Оформить заказ</button>
            @else
            <a href="{{ route('checkout.index') }}" class="hb-btn hb-btn-lg" id="proceed">Оформить заказ</a>
            @endif

            @if(!isset($checkout))
                </form>
            @endif
    </div>
</div>
