@extends('layouts.app', [
    'title' => 'Корзина'
])
@section('breadcrumbs')
    <div id="hb-innerbanner" class="hb-innerbanner hb-haslayout">
        <div class="containers">
            <div class="hb-innerbanner-area">
                <div class="hb-bannarheading">
                    <h1>Магазин</h1>
                </div>
                <ul class="hb-breadcrumb">
                    <li><a href="{{ route('home') }}">Главная</a></li>
                    <li>Магазин</li>
                </ul>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <section>
        <div class="container">
            <div class="row order">
                <div class="col-12 col-md-8">
                    <div class="product-table">
                        <form action="{{route('cart.update')}}" method="post" name="updateCartItem">
                            @csrf
                            <table>
                                <thead>
                                <tr>
                                    <th class="table-header">Товар</th>
                                    <th class="table-header">Цена</th>
                                    <th class="table-header">Количество</th>
                                    <th class="table-header">Сумма</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($cart = Cart::getContent() as $cartItem)
                                    <tr>
                                        <td class="table-item">
                                            {{ $cartItem->name }}
                                            <a style="padding: 0" href="{{route('cart.delete', $cartItem->id)}}">
                                                <i class="ti-close"></i>
                                            </a>
                                        </td>
                                        <td class="table-item">{{$cartItem->price}}</td>
                                        <td class="table-item">
                                            <div class="input-group number-spinner">
                                                <span class="input-group-btn">
                                                    <button
                                                        type="button"
                                                        class="btn btn-default"
                                                        data-dir="dwn">-</button>
                                                </span>
                                                <input
                                                    type="text"
                                                    readonly
                                                    class="form-control text-center"
                                                    name="item[{{$cartItem->id}}]"
                                                    value={{$cartItem->quantity}}
                                                />
                                                <span class="input-group-btn">
                                                    <button
                                                        type="button"
                                                        class="btn btn-default"
                                                        data-dir="up">+</button>
                                                </span>
                                            </div>
                                        </td>
                                        <td class="table-item">{{$cartItem->quantity*$cartItem->price}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <span>
                                <i class="fas fa-sync-alt"></i>
                                <button type="submit" class="btn btn-link">Обновить корзину</button>
                            </span>
                        </form>
                    </div>
                </div>

<!--
                <div class="col-12 col-md-4">
                    @include('cart._order-info')
                </div>
-->
           
 <div class="order-information">
    <div class="cart">
        <form>
            <a href="{{ route('checkout.index') }}" class="hb-btn hb-btn-lg" id="proceed">Оформить заказ</a>
        </form>
    </div>
</div>
           
           
            </div>
        </div>
    </section>
@endsection
