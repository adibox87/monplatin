@extends('layouts.app', [
    'title' => 'Mon Platin Center'
])

@section('breadcrumbs')
    <!--************************************
				Home Slider v2 Start
		*************************************-->
    <div id="hb-homeslider2" class="hb-homeslider v2 owl-carousel owl-theme hb-haslayout">
        @foreach($slides as $slide)
            <div class="item">
                <div class="container-fluid" style="padding: 0; max-height: 500px; width: auto;">
{{--                    <div class="row">--}}
{{--                        <div class="col-xs-12">--}}
{{--                            <div class="hb-slidercontent-area">--}}
{{--                                <figure class="hb-sliderimg">--}}
{{--                                    <img src="{{ Voyager::image($slide->image)  }}" alt="{{ $slide->title }}">--}}
{{--                                    <figcaption class="hb-slidercontent">--}}
{{--                                        <h2>--}}
{{--                                            {{ $slide->title }}--}}
{{--                                        </h2>--}}
{{--                                        <div class="hb-slider-description">--}}
{{--                                            <p>{{ $slide->text }}</p>--}}
{{--                                        </div>--}}
{{--                                        <a href="{{ url()->to($slide->url) }}" class="hb-btn">{{ $slide->text_button }}</a>--}}
{{--                                    </figcaption>--}}
{{--                                </figure>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}

                    <a href="{{ url()->to($slide->url) }}">
{{--                        <img src="https://image-placeholder.com/images/actual-size/1920x1080.png" alt="" style="width: 100%; height: 100%;">--}}
                        <img src="{{ Voyager::image($slide->image)  }}" alt="{{ $slide->title }}"  alt="" style="width: 100%; height: 100%;">

                    </a>

                </div>
            </div>
        @endforeach
    </div>
    <!--************************************
            Home Slider V2 End
    *************************************-->
@endsection


@section('content')
    <!--************************************
                Paradise Center Start
    *************************************-->
    <section id="hb-paradisecenter" class="hb-paradisecenter hb-sectionspace hb-haslayout">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-sm-offset-0 col-md-8 col-md-offset-2">
                    <div class="hb-sectionhead mb-50">
                        <div class="hb-sectiontitle">
                            <h1>Mon Platin Center</h1>
                        </div>
                        <div class="hb-headcontent">
                            <h2>Немного о нас</h2>
                            <div class="hb-description">
                                <p>
                                    «Мон Платин Центр» — это фирменный салон-магазин, в котором можно не только
                                    приобрести косметические средства известного бренда Mon Platin,
                                    но и сделать шикарную стрижку, укладку, окраску или воспользоваться эксклюзивными
                                    процедурами, направленными на оздоровление и лечение волос.
                                </p>
                                <p>
                                    Концепция «Мон Платин Центр» заключается в превосходном сочетании салона красоты с
                                    фирменным магазином косметики.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="hb-paradisecenter-area hb-haslayout">
                    <div class="col-xs-12 col-sm-6 col-md-3">
                        <div class="hb-paradisecenterbox">
									<span class="hb-paradiseiconbox">
										<i class="ti-heart-broken"></i>
									</span>
                            <div class="hb-paradisecontent">
                                <h3 class="hb-headingtree" style="height: 25px">Достижения</h3>
                                <div class="hb-description">
                                    <p>Косметика Мон Платин признана более чем в 30 странах мира</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-3">
                        <div class="hb-paradisecenterbox">
									<span class="hb-paradiseiconbox">
										<i class="ti-user"></i>
									</span>
                            <div class="hb-paradisecontent">
                                <h3 class="hb-headingtree" style="height: 25px">Специалисты</h3>
                                <div class="hb-description">
                                    <p>У нас работают только высококвалифицированные специалисты</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-3">
                        <div class="hb-paradisecenterbox">
									<span class="hb-paradiseiconbox">
										<i class="ti-face-smile"></i>
									</span>
                            <div class="hb-paradisecontent">
                                <h3 class="hb-headingtree" style="height: 25px">Хорошее настроение</h3>
                                <div class="hb-description">
                                    <p>Мы подарим Вам хорошее настроение :)</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-3">
                        <div class="hb-paradisecenterbox">
									<span class="hb-paradiseiconbox">
										<i class="ti-credit-card"></i>
									</span>
                            <div class="hb-paradisecontent">
                                <h3 class="hb-headingtree" style="height: 25px">Фирменный магазин</h3>
                                <div class="hb-description">
                                    <p>В нашем фирменном магазине Вы найдёте полный перечень косметики
                                        «Mon Platin» по уходу за волосами, лицом и телом</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--************************************
                Paradise Center End
    *************************************-->


    <section id="hb-blog" class="hb-blog hb-sectionspace hb-haslayout">
        <div class="container">
            <div class="row">
                <div class=" col-xs-12 col-sm-12 col-sm-offset-0 col-md-8 col-md-offset-2">
                    <div class="hb-sectionhead">
                        <div class="hb-sectiontitle">
                            <h2><span>Mon Platin Center</span>
                                Популярные товары
                            </h2>
                        </div>
                    </div>
                </div>
                <div class="hb-post-area">
                    @foreach($popularProducts as $product)
                        <div class="col-xs-12 col-sm-3 col-lg-3 mb-20">
                            @include('partials.product')
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>

{{--    <section id="hb-services" class="hb-services hb-sectionspace v2 hb-bg hb-haslayout">--}}
{{--        <div class="container">--}}
{{--            <div class="row">--}}
{{--                <div class=" col-xs-12 col-sm-12 col-sm-offset-0 col-md-8 col-md-offset-2">--}}
{{--                    <div class="hb-sectionhead">--}}
{{--                        <div class="hb-sectiontitle">--}}
{{--                            <h2><span>Mon Platin Center</span>--}}
{{--                                Наши услуги--}}
{{--                            </h2>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="hb-post-area">--}}
{{--                    @foreach($salons as $salon)--}}
{{--                        <div class="row mt-50">--}}
{{--                            <div class="col-xs-12">--}}
{{--                                <h3 class="text-center">--}}
{{--                                    <span>{{ $salon->title }}</span>--}}
{{--                                </h3>--}}
{{--                            </div>--}}

{{--                            @foreach($salon->salonServiceCategories as $serviceCategory)--}}
{{--                                <div class="col-xs-12 col-sm-4 col-md-4 mt-40">--}}
{{--                                    <div class="service">--}}
{{--                                        <div class="service__image">--}}
{{--                                            <img src="{{ Voyager::image($serviceCategory->image) }}" alt="{{ $serviceCategory->title }}" />--}}
{{--                                        </div>--}}
{{--                                        <div class="service__description">--}}
{{--                                            <h3 class="service__title">--}}
{{--                                                <a href="{{ route('service-category.show', [$serviceCategory->slug]) }}">--}}
{{--                                                    {{ $serviceCategory->title }}--}}
{{--                                                </a>--}}
{{--                                            </h3>--}}
{{--                                            <span class="service__price">от {{ $serviceCategory->min_price }} р.</span>--}}
{{--                                            <a--}}
{{--                                                href="{{ route('service-category.show', [$serviceCategory->slug]) }}"--}}
{{--                                                class="hb-btn service__button"--}}
{{--                                            >--}}
{{--                                                Подробнее--}}
{{--                                            </a>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            @endforeach--}}
{{--                        </div>--}}
{{--                    @endforeach--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </section>--}}

    @if($saleProducts->count() > 0)
        <section id="hb-blog" class="hb-blog hb-sectionspace hb-haslayout">
            <div class="container">
                <div class="row">
                    <div class=" col-xs-12 col-sm-12 col-sm-offset-0 col-md-8 col-md-offset-2">
                        <div class="hb-sectionhead">
                            <div class="hb-sectiontitle">
                                <h2><span>Mon Platin Center</span>
                                    Товары по акции
                                </h2>
                            </div>
                        </div>
                    </div>
                    <div class="hb-post-area">
                        @foreach($saleProducts as $product)
                            <div class="col-xs-12 col-sm-3 col-lg-3 mb-20">
                                @include('partials.product')
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </section>
    @endif


    <!--************************************
                Blog Start
    *************************************-->
    <section id="hb-blog" class="hb-blog hb-sectionspace hb-haslayout">
        <div class="container">
            <div class="row">
                <div class=" col-xs-12 col-sm-12 col-sm-offset-0 col-md-8 col-md-offset-2">
                    <div class="hb-sectionhead">
                        <div class="hb-sectiontitle">
                            <h2><span>Mon Platin Center</span>
                                Новости и события
                            </h2>
                        </div>
                    </div>
                </div>
                <div class="hb-post-area">
                    @foreach($articles as $article)
                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                            <article class="hb-post">
                                <figure class="hb-postimage">
                                    <a href="{{ url('/articles/'.$article->articleCategory->slug.'/'.$article->slug) }}">
                                        <img src="storage/{{ $article->image}}" alt="{{ $article->title }}">
                                    </a>
                                </figure>
                                <div class="hb-postcontent">
                                    <a href="{{ route('article.articles', ['category' => $article->articleCategory->slug]) }}"
                                       class="hb-postcategory"
                                    >
                                        {{ $article->articleCategory->name }}
                                    </a>
                                    <div class="hb-posttitle">
                                        <h3>
                                            <a href="{{ url('/articles/'.$article->articleCategory->slug.'/'.$article->slug) }}">{{ $article->title }}</a>
                                        </h3>
                                    </div>
                                    <a href="{{ url('/articles/'.$article->articleCategory->slug.'/'.$article->slug) }}">
                                    <div class="hb-post-area">
                                        <span>
                                            {{ $article->small_desc }}
                                        </span>
                                    </div></a>
<!--
                                    <div class="hb-postmeta" style="margin-top: 25px;">
                                        <span>{{ $article->created_at->diffForHumans() }}</span>
                                    </div>
-->
                                </div>
                            </article>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
        <script type="text/javascript" src="https://w309180.yclients.com/widgetJS" charset="UTF-8"></script>
    <!--************************************
                Blog End
    *************************************-->

    @include('portfolio.section', ['portfolios' => $portfolios])
@endsection

@push('html-body-bottom')
    <script lang="text/javascript">
        $(function () {
            $('#masonry').isotope({itemSelector: '.masonry-grid', filter: selector});
        })
    </script>
@endpush
