@extends('layouts.app', [
    'title' => 'Лучшие салоны - ' . $salon->title
])

@section('breadcrumbs')
    <div id="hb-innerbanner" class="hb-innerbanner hb-haslayout">
        <div class="containers">
            <div class="hb-innerbanner-area">
                <div  class="hb-bannarheading">
                    <h1>{{ $salon->title }}</h1>
                </div>
                <ul class="hb-breadcrumb">
                    <li><a href="{{ route('home') }}">Главная</a></li>
                    <li><a href="{{ route('salon.index') }}">Салоны</a></li>
                    <li>{{ $salon->title }}</li>
                </ul>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <section id="hb-services" class="hb-services v2 hb-bg hb-haslayout hb-sectionspace">
        <div class="container">
            <div class="row">
                <div class=" col-xs-12 col-sm-12 col-sm-offset-0 col-md-8 col-md-offset-2">
                    <div class="hb-sectionhead">
                        <div class="hb-sectiontitle">
                            <h2><span>Mon Platin Center</span>
                                Наши услуги
                            </h2>
                        </div>
                    </div>
                </div>
                <div class="hb-post-area">
                    @foreach($categories as $serviceCategory)
                        <div class="col-xs-12 col-sm-4 col-md-4 mt-40">
                            <div class="service">
                                <div class="service__image">
                                    <img src="{{ Voyager::image($serviceCategory->image) }}" alt="{{ $serviceCategory->title }}" />
                                </div>
                                <div class="service__description">
                                    <h3 class="service__title">
                                        <a href="{{ route('service-category.show', [$serviceCategory->slug]) }}">
                                            {{ $serviceCategory->title }}
                                        </a>
                                    </h3>
                                    <span class="service__price">от {{ $serviceCategory->min_price }} р.</span>
                                    <a
                                        href="{{ route('service-category.show', [$serviceCategory->slug]) }}"
                                        class="hb-btn service__button"
                                    >
                                        Подробнее
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
    <script type="text/javascript" src="https://w309180.yclients.com/widgetJS" charset="UTF-8"></script>
    @include('portfolio.section', ['portfolios' => $portfolios])

    <section id="hb-gallery" class="hb-gallery v2 hb-haslayout mb-50">
        <div class="container">
            <div class="row">
                <div class=" col-xs-12 col-sm-12 col-sm-offset-0 col-md-8 col-md-offset-2">
                    <div class="hb-sectionhead mb-50">
                        <div class="hb-sectiontitle">
                            <h2><span>Mon Platin Center</span>
                                Видео
                            </h2>
                        </div>
                    </div>
                </div>
                <div class=" col-xs-12">
                    <div class="hb-gallery-area">
                        <div class="hb-portfolio-content hb-haslayout">
                            @foreach($salon->salonVideos as $video)
                                <div class="col-xs-12 col-sm-6 col-md-6 mt-25">
                                    <iframe
                                        id="ytplayer"
                                        type="text/html"
                                        width="640"
                                        height="360"
                                        src="http://www.youtube.com/embed/{{ $video->url }}?autoplay=0&origin={{ env('APP_URL') }}"
                                        frameborder="0">
                                    </iframe>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="hb-team" class="hb-team hb-bg hb-haslayout">
        <div class="container">
            <div class="row">
                <div class=" col-xs-12 col-sm-12 col-sm-offset-0 col-md-8 col-md-offset-2">
                    <div class="hb-sectionhead mt-50">
                        <div class="hb-sectiontitle">
                            <h2><span>Mon Platin Center</span>
                                Наши мастера
                            </h2>
                        </div>
                    </div>
                </div>
                <div class="hb-teamarea hb-haslayout">
                    @foreach($salon->salonUsers as $user)
                        <div class="col-xs-12 col-sm-6 col-md-3 mt-25">
                            <div class="hb-teambox">
                                <figure class="hb-teamimg">
                                    <img src="/storage/{{ $user->image }}" alt="{{ $user->name }}">
                                    <figcaption class="hb-imagecontent">
                                    </figcaption>
                                </figure>
                                <div class="hb-teamcontent">
                                    <h3 class="hb-headingtree">{{ $user->name }}
                                        <span>{{ $user->position }}</span>
                                    </h3>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>


    @if (!empty($salon->description))
        <section id="hb-blog" class="hb-blog hb-haslayout service">
        <div class="container">
            <div class="row">
                <div class=" col-xs-12 col-sm-12 col-sm-offset-0 col-md-8 col-md-offset-2">
                    <div class="hb-sectionhead mt-50">
                        <div class="hb-sectiontitle">
                            <h2><span>Mon Platin Center</span>
                                О салоне
                            </h2>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="hb-post-area hb-blogdetails">
                        <article class="hb-post">
                            <div class="hb-description">
                                {!! $salon->description !!}
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @endif
@endsection
