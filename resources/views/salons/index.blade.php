@extends('layouts.app', [
    'title' => 'Салоны'
])

@section('breadcrumbs')
    <div id="hb-innerbanner" class="hb-innerbanner hb-haslayout">
        <div class="containers">
            <div class="hb-innerbanner-area">
                <div class="hb-bannarheading">
                    <h1>Салоны Mon Platin</h1>
                </div>
                <ul class="hb-breadcrumb">
                    <li><a href="{{ route('home') }}">Главная</a></li>
                    <li>Салоны Mon Platin</li>
                </ul>
            </div>
        </div>
    </div>
@endsection()

@section('content')
    <!--************************************
                    Blog Start
        *************************************-->
    <section id="hb-blog" class="hb-blog hb-sectionspace hb-haslayout">
        <div class="container">
            <div class="row">
                <div class="hb-post-area hb-bloggridfullwidth">
                    @foreach($salons as $salon)
                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-6">
                            <article class="hb-post text-center">
                                <figure class="hb-postimagesalon">
                                    <a href="salons/{{ $salon->slug }}"><img src="/storage/{{ $salon->image }}"
                                                                             alt="image description"></a>
                                </figure>
                                <div class="hb-postcontent">
                                    <div class="hb-posttitle">
                                        <h3><a href="{{ route('salon.view', $salon->slug) }}">{{ $salon->title }}</a>
                                        </h3>
                                    </div>
                                    <div class="hb-postmeta">
                                        <span>{{ $salon->small_description }}</span>
                                    </div>
                                    <div class="hb-postmeta">
                                        Время работы: <span>{{ $salon->work_time }}</span>
                                    </div>
                                    <div class="hb-postmeta">
                                        Адрес: <span>{{ $salon->address }}</span>
                                    </div>
                                    <a href="{{ route('salon.view', $salon->slug) }}"
                                       class="hb-btn hidden-xs">Перейти</a>
                                </div>
                            </article>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
    <script type="text/javascript" src="https://w309180.yclients.com/widgetJS" charset="UTF-8"></script>
    <!--************************************
                Blog End
    *************************************-->
@endsection
