@extends('layouts.app', [
    'title' => 'Заказы'
])
@section('content')
    <section class="hb-error hb-sectionspace hb-haslayout">
        <div class="container">
            <div class="row">
                <div class="col-md-3 mb-50">
                    <div>
                        <div>
                            <nav class="display-1">
                                <ul class="list-unstyled">
                                    <li><h2>{{ auth('customer')->user()->name }}</h2></li>
                                    <li><h3><a href="{{ route('customer.dashboard') }}">Мой профиль</a></h3></li>
                                    <li><h3><a href="{{ route('customer.orders') }}">Заказы</a></h3></li>
                                    <li><h3><a href="{{ route('customer.logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"
                                            >Выйти</a></h3></li>
                                    <form id="logout-form" action="{{ route('customer.logout') }}" method="POST"
                                          style="display: none;">
                                        @csrf
                                    </form>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
                <div class="col-md-9">
                    <table>
                        <thead>
                        <tr>
                            <th class="table-header" style="width: 6rem;">ID</th>
                            <th class="table-header">Дата заказа</th>
                            <th class="table-header">Итого</th>
                            <th class="table-header"></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($orders as $order)
                            <tr>
                                <td class="table-item">{{ $order->id }}</td>
                                <td class="table-item">{{ optional($order->created_at)->formatLocalized('%d %B %Y в %H:%M') }}</td>
                                <td class="table-item">{{ $order->total }} руб.</td>
                                <td class="table-item">
                                    <a href="{{ URL::signedRoute('checkout.view', $order) }}">Подробнее</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>


@endsection


