<div class="row address">
    <div class="col-xs-12 col-md-6">
        <div class="form-group">
            <label for="country_id">Страна <strong>*</strong></label>
            <select
                class="form-control"
                id="country_id"
                name="country_code"
                autocomplete="country_code"
                readonly>
                <option value="BY" selected>Республика Беларусь</option>
            </select>
        </div>
    </div>
    <div class="col-xs-12 col-md-6">
        <div class="form-group">
            <label for="state">Область <strong>*</strong></label>
            <select
                class="form-control"
                id="state"
                autocomplete="address-level1"
                name="state">
                <option>Минская область</option>
                <option>Брестская область</option>
                <option>Гомельская область</option>
                <option>Гродненская область</option>
                <option>Могилевская область</option>
                <option>Витебская область</option>
            </select>
        </div>
    </div>
    <div class="col-xs-12">
        <div class="form-group">
            <label for="street">Адрес <strong>*</strong></label>
            <input
                type="text"
                class="form-control mb-10"
                id="street"
                name="street"
                value="{{ old('street') ?? optional($address)->street }}"
                autocomplete="street-address"
                placeholder="Номер дома и название улицы"
                required>
            <input
                type="text"
                class="form-control"
                id="street_extra    "
                name="street_extra"
                value="{{ old('street_extra') ?? optional($address)->street_extra }}"
                autocomplete="street_extra"
                placeholder="Доп.сведения (необязательно)">
        </div>
    </div>
    <div class="col-xs-12 col-md-6">
        <div class="form-group">
            <label for="city">Населенный пункт <strong>*</strong></label>
            <input
                type="text"
                class="form-control"
                id="city"
                name="city"
                value="{{ old('city') ?? optional($address)->city }}"
                autocomplete="address-level2"
                placeholder="Населенный пункт"
                required>
        </div>
    </div>
    <div class="col-xs-12 col-md-6">
        <div class="form-group">
            <label for="post_code">Почтовый индекс <strong>*</strong></label>
            <input
                type="text"
                class="form-control"
                id="post_code"
                name="post_code"
                pattern="[0-9]{6}"
                value="{{ old('post_code') ?? optional($address)->post_code }}"
                autocomplete="postal-code"
                inputmode="tel"
                required>
        </div>
    </div>
</div>
