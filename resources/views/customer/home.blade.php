@extends('layouts.app', [
    'title' => 'Личный кабинет'
])
@section('content')
    <section class="hb-error hb-sectionspace hb-haslayout">
        <div class="container">
            <div class="row">
                <div class="col-md-3 mb-50">
                    <div>
                        <div>
                            <nav class="display-1">
                                <ul class="list-unstyled">
                                    <li><h2>{{$customer->name}}</h2></li>
                                    <li><h3><a href="{{ route('customer.dashboard') }}">Мой профиль</a></h3></li>
                                    <li><h3><a href="{{ route('customer.orders') }}">Заказы</a></h3></li>
                                    <li><h3><a href="{{ route('customer.logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"
                                            >Выйти</a></h3></li>
                                    <form id="logout-form" action="{{ route('customer.logout') }}" method="POST"
                                          style="display: none;">
                                        @csrf
                                    </form>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="personal-data">
                        <h3 class="mb-30">Мой профиль</h3>
                        <h5>Личные данные</h5>

                        @if(session()->has('message'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                {{ session()->get('message') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif

                        <form action="{{url('customer/update')}}" method="post">
                            @csrf
                            <div class="row">
                                <div class="col-xs-12 col-md-6">
                                    <div class="form-group">
                                        <label for="name">Имя</label>
                                        <input
                                            type="text"
                                            class="form-control"
                                            id="name"
                                            name="name"
                                            value="{{ old('name') ?? $customer->name }}"
                                            autocomplete="given-name"
                                            placeholder="Имя получателя">
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-6">
                                    <div class="form-group">
                                        <label for="surname">Фамилия</label>
                                        <input
                                            type="text"
                                            class="form-control"
                                            id="surname"
                                            name="surname"
                                            value="{{ old('surname') ?? $customer->surname }}"
                                            autocomplete="family-name"
                                            placeholder="Фамилия получателя">
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-6">
                                    <div class="form-group">
                                        <label for="phone">Телефон (+375-XX-XXX-XX-XX)</label>
                                        <input
                                            class="form-control"
                                            id="phone"
                                            name="phone"
                                            value="{{ old('phone') ?? $customer->phone }}"
                                            autocomplete="tel"
                                            type="tel"
                                            pattern="\+375-[0-9]{2}-[0-9]{3}-[0-9]{2}-[0-9]{2}"
                                            inputmode="tel">
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-6">
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input
                                            type="email"
                                            class="form-control"
                                            id="email"
                                            name="email"
                                            value="{{ old('email') ?? $customer->email }}"
                                            autocomplete="email"
                                            placeholder="Email">
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary">Сохранить</button>
                        </form>

                        <h5 class="mt-30">Мой адрес</h5>
                        @php
                            $address = $customer->getPrimaryAddress()
                        @endphp
                        <form action="{{ url('customer/address') }}" method="post">
                            @csrf
                            @include('customer.address', ['address' => $address])
                            <button type="submit" class="btn btn-primary">Сохранить</button>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection


