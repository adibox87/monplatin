<div class="row">
    <div class="col-md-12">
        <p class="text-center"><strong>Выберите пункт самовывоза</strong></p>
{{--        @foreach($salons as $salon)--}}
{{--            <div class="form-check">--}}
{{--                <label for="cherrypick{{ $salon->id }}">--}}
{{--                    <input id="cherrypick{{ $salon->id }}" required class="form-check-input" type="radio" name="salon_id" value="{{ $salon->id }}">--}}
{{--                    {{ $salon->title }}--}}
{{--                </label>--}}
{{--            </div>--}}
{{--        @endforeach--}}

        <select name="salon_id" id="salon_id" required style="max-width: 100%" >
            <option value>Выбрать салон</option>

            @foreach($salons as $salon)
                <option value="{{$salon->id}}"> {{ $salon->title }}  </option>
            @endforeach

        </select>

    </div>
</div>
