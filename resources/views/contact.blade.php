@extends('layouts.app', [
    'title' => 'Контакты'
])

@section('breadcrumbs')
    <div id="hb-innerbanner" class="hb-innerbanner hb-haslayout">
        <div class="containers">
            <div class="hb-innerbanner-area">
                <div class="hb-bannarheading">
                    <h1>Контакты</h1>
                </div>
                <ul class="hb-breadcrumb">
                    <li><a href="{{ route('home') }}">Главная</a></li>
                    <li>Контакты</li>
                </ul>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <!--************************************
                    Paradise Center Start
        *************************************-->
    <section id="hb-contactus" class="hb-contactus hb-sectionspace hb-haslayout">
        <div class="container">
            <div class="row">
                <div class="hb-sectiontitle">
                    <h2><span>Mon Platin Center</span>
                        С нами Вы будете неотразимы!
                    </h2>
                </div>
                @foreach($salons as $salon)
                    <div class="col-xs-12 col-sm-12 col-sm-offset-0 col-md-6">
                        <div class="hb-sectionhead mb-50">
                            <div class="hb-headcontent">
                                <h2>{{ $salon->title }}</h2>
                                <div class="hb-description">
                                    <p>
                                        <b>Мы находимся по адресу:</b>
                                    </p>
                                    <p>{{ $salon->address }}</p>
                                    <b>Телефон для справок:</b>
                                    <p>
                                        @foreach(explode("\n", $salon->phones) as $phone)
                                            <a href="tel:{{ preg_replace('/\D/', '', $phone) }}">
                                                {{ $phone }}
                                            </a></br>
                                        @endforeach
                                    </p>
                                    <b>Время работы:</b>
                                    <p>{{ $salon->work_time }}</p>
                                    <b>E-mail:</b>
                                    <a href="mailto:{{ $salon->email }}">{{ $salon->email }}</a>
                                </div>

                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="hb-contactus-area hb-haslayout">
                <div class="col-xs-12 col-sm-12">
                    <figure class="hb-contactusmap">
                        <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A448d9fa07d1a5c799a1215506eff61442ae3e0984bddf0ce1b680eae7bd2afa5&amp;width=100%25&amp;height=350&amp;lang=ru_RU&amp;scroll=true"></script>
                    </figure>
                </div>
                <div class="hb-addressarea">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="hb-address">
                            <h2>Реквизиты</h2>
                            <div class="hb-addressbox">
                                <p><b>ООО «Вест»</b>, г. Жодино, ул. Калиновского, 7</p>
                                <p>Св-во о регистрации №237 от 10.04.2001 г.</p>
                                <p>Сайт Зарегистрирован в Торговом реестре РБ</p>
                                <p>за №303013 28.01.2016 г.</p>
                                <p>Жодинским городским исполнительным комитетом</p>
                                <p>Юридический адрес: 222160, г .Жодино</p>
                                <p>ул. Калиновского, 7</p>
                                <p>ИНН: 600040287</p>
                                <p>Банк получателя: ЦБУ №616 филиала №612 ОАО «АСБ Беларусбанк» г. Жодино ул. 50 лет
                                    октября, 25-а</p>
                                <p>Расчетный счет: BY41 AKBB 3012 0616 0169 4620 0000 BYN БИК AKBBBY21612</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>
@endsection
