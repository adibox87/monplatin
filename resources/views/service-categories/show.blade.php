@extends('layouts.app', [
    'title' => $serviceCategory->title
])

@section('breadcrumbs')
    <div id="hb-innerbanner" class="hb-innerbanner hb-haslayout">
        <div class="containers">
            <div class="hb-innerbanner-area">
                <div class="hb-bannarheading">
                    <h3 style="color: white;">{{ $serviceCategory->title }}</h3>
                </div>
                <ul class="hb-breadcrumb">
                    <li><a href="{{ route('home') }}">Главная</a></li>
                    <li>{{ $serviceCategory->title }}</li>
                </ul>
            </div>
        </div>
    </div>
        <script type="text/javascript" src="https://w309180.yclients.com/widgetJS" charset="UTF-8"></script>
@endsection


@section('content')
    <section id="hb-services" class="hb-services v2 hb-bg hb-haslayout hb-sectionspace">
        @if ($errors->any())
            <div class="container">
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        @endif
        <div class="container">
            <div class="row">
                <div class=" col-xs-12 col-sm-12 col-sm-offset-0 col-md-8 col-md-offset-2">
                    <div class="hb-sectionhead">
                        <div class="hb-sectiontitle">
                            <h2><span>Mon Platin Center</span>
                                {{ $serviceCategory->title }}
                            </h2>
                        </div>
                    </div>
                </div>
                <div class="hb-post-area">
                    @foreach($salons as $salon)
                        <div class="row mt-50">
                            <div class="col-xs-12">
                                <h3 class="text-center">
                                    <span>{{ $salon->title }}</span>
                                </h3>
                            </div>

                            @foreach($salon->salonServices->where('salon_service_category_id', $serviceCategory->id) as $service)
                                <div class="col-xs-12 col-sm-6 col-md-3 mt-40">
                                    <div class="hb-servicebox">
                                        <div class="hb-servicecontentprice"  style="min-height: 285px">
                                            <figure class="hb-serviceprice">
                                                <h2>{{ $service->price }} р.</h2>
                                            </figure>
                                            <h3 class="hb-headingtree">
                                                <a href="{{ route('service-category.service', [$serviceCategory, $service]) }}">
                                                    {{ $service->title }}
                                                </a>
                                            </h3>
                                            <a
                                                href="#"
                                                data-modal="#modal-service-{{$service->id}}"
                                                class="hb-btn modal__trigger"
                                            >
                                                Записаться
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>

    @if (!empty($serviceCategory->description))
        <section id="hb-blog" class="hb-blog hb-haslayout service">
            <div class="container">
                <div class="row">
                    <div class=" col-xs-12 col-sm-12 col-sm-offset-0 col-md-8 col-md-offset-2">
                        <div class="hb-sectionhead mt-50">
                            <div class="hb-sectiontitle">
                                <h2><span>Mon Platin Center</span>
                                    Об услуге
                                </h2>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="hb-post-area hb-blogdetails">
                            <article class="hb-post">
                                <div class="hb-description">
                                    {!! $serviceCategory->description !!}
                                </div>
                            </article>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    @endif

    @foreach($services as $service)
        <div id="modal-service-{{ $service->id }}" class="modal modal__bg" role="dialog" aria-hidden="true">
            <div class="modal__dialog">
                <div class="modal__content">
                    <h2 style="padding: 20px; text-align: center">{{ $service->title }}</h2>
                    <form
                        class="row form-contact contact_form"
                        action="{{ route('salon.service.record') }}"
                        method="post"
                        id="contactFormService{{ $service->id }}"
                    >
                        @csrf
                        <div class="col-sm-12">
                            <div class="form-group">
                                Введите Ваше имя
                                <input
                                    class="form-control"
                                    name="name"
                                    id="name"
                                    type="text"
                                    placeholder="Имя"
                                    required
                                >
                            </div>
                            <div class="form-group">
                                Введите Ваш номер
                                <input
                                    required
                                    class="form-control"
                                    name="phone"
                                    id="number"
                                    type="text"
                                    placeholder="+375-(XX)-XXX-XX-XX"
                                >
                            </div>
                            <div class="form-group hidden">
                                Введите Ваш номер
                                <input
                                    required
                                    hidden
                                    class="form-control"
                                    value="{{ $service->id }}"
                                    name="service_id"
                                    id="serivce_id"
                                    type="text"
                                >
                            </div>
                            <div class="form-group">
                                Выберите салон
                                <select
                                    required
                                    class="form-control"
                                    name="service_salon"
                                    id="service_salon"
                                >
                                    @foreach($service->salons as $salon)
                                        <option value="{{ $salon->id }}">{{ $salon->title }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group mt-lg-3">
                                <button style="cursor: pointer;" onclick="form_submitService{{ $service->id }}()"
                                        type="submit" class="hb-btn button-contactFormСontact">Отправить заявку
                                </button>
                            </div>
                        </div>
                    </form>
                    <!-- modal close button -->
                    <a href="" class="modal__close demo-close">
                        <svg class="" viewBox="0 0 24 24">
                            <path
                                d="M19 6.41l-1.41-1.41-5.59 5.59-5.59-5.59-1.41 1.41 5.59 5.59-5.59 5.59 1.41 1.41 5.59-5.59 5.59 5.59 1.41-1.41-5.59-5.59z"/>
                            <path d="M0 0h24v24h-24z" fill="none"/>
                        </svg>
                    </a>

                </div>
            </div>
        </div>

        <script type="text/javascript">
            function form_submitService{{ $service->id }}() {
                document.getElementById("contactFormService{{ $service->id }}").submit();
            }
        </script>
    @endforeach
@endsection
