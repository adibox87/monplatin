@extends('layouts.app', [
    'title' => $service->title
])

@section('breadcrumbs')
    <div id="hb-innerbanner" class="hb-innerbanner hb-haslayout">
        <div class="containers">
            <div class="hb-innerbanner-area">
                <div class="hb-bannarheading">
                    <h3 style="color: white;">{{ $service->title }}</h3>
                </div>
                <ul class="hb-breadcrumb">
                    <li><a href="{{ route('home') }}">Главная</a></li>
                    <li><a href="{{ route('salon.index') }}">Салоны</a></li>
                    <li><a href="{{ route('salon.view', [$service->salons->first()]) }}">{{ $service->salons->first()->title }}</a></li>
                    <li><a href="{{ route('service-category.show', [$serviceCategory]) }}">{{ $serviceCategory->title }}</a></li>
                    <li>{{ $service->title }}</li>
                </ul>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <section id="hb-blog" class="hb-blog hb-sectionspace hb-haslayout service">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="hb-post-area hb-blogdetails">
                        <article class="hb-post">
                            <div class="hb-postcontent">
                                <h1>{{ $service->title }}</h1>
                            </div>
                            <figure class="hb-postimage">
                                <img src="/storage/{{$service->image}}" alt="image description" style="width: 330px;">
                            </figure>
                            <div class="hb-description">
                                {!! $service->description !!}
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </div>
    </section>
        <script type="text/javascript" src="https://w309180.yclients.com/widgetJS" charset="UTF-8"></script>
@endsection
