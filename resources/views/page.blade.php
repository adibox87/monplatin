@extends('layouts.app', [
    'title' => $page->seo_title,
    'description' => $page->seo_description
])

@section('breadcrumbs')
    <div id="hb-innerbanner" class="hb-innerbanner hb-haslayout">
        <div class="containers">
            <div class="hb-innerbanner-area">
                <div  class="hb-bannarheading">
                    <h1>{{ $page->title }}</h1>
                </div>
                <ul class="hb-breadcrumb">
                    <li><a href="{{ route('home') }}">Главная</a></li>
                    <li>{{ $page->title }}</li>
                </ul>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <section id="hb-error" class="hb-error hb-sectionspace hb-haslayout">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    {!! $page->body !!}
                </div>
            </div>
        </div>
    </section>
@endsection
