<?php

return [
    'data_rows' => [
        'subtitle' => 'Описание',
        'icon' => 'Иконка',
        'sort' => 'Сортировка',
        'content' => 'Контент',
    ],
    'data_types' => [
        'portfolios' => [
            'singular' => 'Портфолио',
            'plural' => 'Портфолио',
        ],
    ],
    'menu_items' => [
        'portfolios' => 'Портфолио',
        'reviews' => 'Отзывы',
        'others' => 'Другое',
    ],
    'settings' => [
        'site' => [
            'keywords' => 'Ключевые Слова',
            'copyright' => 'Копирайт',
        ],
    ],
];
