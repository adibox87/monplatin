<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'Ваш пароль обновлен',
    'sent' => 'Сообщение отправлено на вашу почту.',
    'throttled' => 'Пожалуйста подождите.',
    'token' => 'Токен недействителен',
    'user' => "Пользователь не найден.",

];
