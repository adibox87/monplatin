let actions = {
    SEARCH_PRODUCTS({commit}, query) {
        let params = {
            query
        };
        axios.get(`/api/search`, {params})
            .then(res => {
                console.log('RES: ', res)
                if (res.statusText === 'OK')
                    console.log('request sent successfully')
                    commit('SET_PRODUCTS', res.data.data)
            }).catch(err => {
            console.log(err)
        })
    },
    GET_PRODUCTS({commit}) {
        axios.get('/api/catalog/products')
            .then(res => {
                {
                    commit('SET_PRODUCTS', res.data.data)
                }
            })
            .catch(err => {
                console.log(err)
            })
    }
}

export default actions