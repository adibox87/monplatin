<?php

use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

Route::group(['prefix' => 'articles'], function () {
    Route::get('/', 'ArticleController@categories')->name('article.categories');
    Route::get('{category}/{article}', 'ArticleController@show')->name('article.article');
});

Route::group(['prefix' => 'salons'], function () {
    Route::get('/', 'SalonController@index')->name('salon.index');
    Route::get('{salon}', 'SalonController@view')->name('salon.view');
    Route::post('/record', 'SalonController@record')->name('salon.service.record');
});

Route::group(['prefix' => 'services', 'as' => 'service-category.'], function () {
    Route::get('{serviceCategory}', 'ServiceCategoryController@show')->name('show');
    Route::get('{serviceCategory}/{service}', 'ServiceCategoryController@service')->name('service');
});

Route::group(['prefix' => 'search'], function () {
    Route::get('/', 'SearchController@index')->name('search.index');
});

Route::group(['prefix' => 'shop'], function () {
    Route::get('/', 'CatalogController@index')->name('catalog.index');
    Route::get('/new', 'CatalogController@new')->name('catalog.new');
    Route::get('/popular', 'CatalogController@popular')->name('catalog.popular');
    Route::get('/sale', 'CatalogController@sale')->name('catalog.sale');
    Route::get('/{category}', 'CatalogController@category')->name('category');
    Route::get('/{category}/{product}/', 'CatalogController@product')->name('product');
});

Route::get('/cart', 'CartController@index')->name('cart.index');
Route::get('/cart/delete/{id}', 'CartController@delete')->name('cart.delete');
Route::get('/cart/add/{id}', 'CartController@add')->name('cart.add');
Route::post('/cart/update', 'CartController@update')->name('cart.update');

Route::get('/checkout', 'CheckoutController@index')->name('checkout.index');
Route::post('/checkout', 'CheckoutController@post')->name('checkout.post');
Route::get('/checkout/{order}.pdf', 'CheckoutController@pdf')->name('checkout.pdf')->middleware(['signed'])->where('order', '[0-9]+');
Route::get('/checkout/{order}/mail', 'CheckoutController@mail')->name('checkout.mail')->middleware(['signed'])->where('order', '[0-9]+');
Route::get('/checkout/{order}', 'CheckoutController@view')->name('checkout.view')->middleware(['signed'])->where('order', '[0-9]+');
Route::post('/checkout/delivery-update', 'CheckoutController@updateDelivery')->name('checkout.deliveryUpdate');

Route::post('/record', 'CatalogController@createRecord')->name('product.store');

Route::get('/contacts', 'ContactController@index')->name('contact');

Route::post('/contacts', 'ContactController@store')->name('contact.store');

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Route::get('gallery', 'PortfolioController@index')->name('portfolio.index');

Route::group(['prefix' => 'customer'], function () {
    Route::group(['namespace' => 'Customer'], function() {
        Route::get('/', 'HomeController@index')->name('customer.dashboard');
        Route::get('/orders', 'HomeController@orders')->name('customer.orders');

        // Login
        Route::get('login', 'Auth\LoginController@showLoginForm')->name('customer.login');
        Route::post('login', 'Auth\LoginController@login');
        Route::post('logout', 'Auth\LoginController@logout')->name('customer.logout');

        // Register
        Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('customer.register');
        Route::post('register', 'Auth\RegisterController@register');

        // Passwords
        Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('customer.password.email');
        Route::post('password/reset', 'Auth\ResetPasswordController@reset');
        Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('customer.password.request');
        Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('customer.password.reset');

        // Must verify email
        Route::get('email/resend','Auth\VerificationController@resend')->name('customer.verification.resend');
        Route::get('email/verify','Auth\VerificationController@show')->name('customer.verification.notice');
        Route::get('email/verify/{id}/{hash}','Auth\VerificationController@verify')->name('customer.verification.verify');

        Route::post('address', 'HomeController@storeAddress')->name('customer.storeAddress');
        Route::post('update', 'HomeController@store')->name('customer.storeAddress');
    });
});

Route::get('{path}', 'RoutableController@show')->where('path', '[a-zA-Z0-9\-/_]+');

// just for sake of url generation

Route::get('{category}', 'ArticleController@category')->name('article.articles');
Route::get('{slug}', [
    'uses' => 'PageController@getPage'
])->where('slug', '([A-Za-z0-9\-\/]+)')->name('pages');
