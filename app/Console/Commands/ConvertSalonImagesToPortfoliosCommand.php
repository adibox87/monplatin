<?php

namespace App\Console\Commands;

use App\Models\Portfolio\Portfolio;
use App\Models\Portfolio\PortfolioCategory;
use App\Models\Salon\SalonImage;
use Illuminate\Console\Command;

class ConvertSalonImagesToPortfoliosCommand extends Command
{
    protected $signature = 'convert:salon-images
                                {--C|category= : Default portfolio category}';

    protected $description = 'Converts SalonImages into Portfolios';

    public function handle()
    {
        $category = PortfolioCategory::findOrFail($this->option('category'));

        if (!$this->confirm('Do you wish to continue?')) {
            exit();
        }

        $salonImages = SalonImage::all();

        $bar = $this->output->createProgressBar(count($salonImages));
        $this->info('Creating Records');
        $bar->start();
        foreach ($salonImages as $salonImage) {
            $category->portfolios()->create([
                'title' => $salonImage->title . ' ' . $salonImage->id,
                'image' => $salonImage->image,
                'type' => Portfolio::TYPE_PHOTO,
            ]);
        }

        $bar->finish();
    }
}
