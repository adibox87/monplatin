<?php

if(!function_exists('mb_strcasecmp')) {
    function mb_strcasecmp($str1, $str2, $encoding = null) {
        if (null === $encoding) {
            $encoding = mb_internal_encoding();
        }
        return strcmp(mb_strtolower($str1, $encoding), mb_strtolower($str2, $encoding));
    }
}

if(!function_exists('utf8_strrev')) {
    function utf8_strrev($str){
        preg_match_all('/./us', $str, $matches);
        return join('', array_reverse($matches[0]));
    }
}
