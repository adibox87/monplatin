<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;

class ProductRecord extends Model
{
    protected $fillable = [
        'name',
        'phone',
        'message',
        'product_id',
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
