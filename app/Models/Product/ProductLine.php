<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class ProductLine extends Model
{
    use HasSlug;

    protected $fillable = [
        'title',
        'slug',
    ];

    public function getSlugOptions()
    {
        return SlugOptions::create()->generateSlugsFrom('title')->saveSlugsTo('slug');
    }
}
