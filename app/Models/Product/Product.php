<?php

namespace App\Models\Product;

use App\Models\Category\Category;
use Illuminate\Database\Eloquent\Model;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class Product extends Model
{
    use HasSlug;

    protected $fillable = [
        'title',
        'slug',
        'description',
        'short_description',
        'view',
        'sale_price',
        'price',
    ];

    public function getSlugOptions()
    {
        return SlugOptions::create()->generateSlugsFrom('title')->saveSlugsTo('slug');
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'product_categories', 'product_id', 'category_id');
    }

    public function getUrl()
    {
        $url = $this->slug;

        $category = $this->categories[0];

        $url = $category->slug.'/'.$url;

        return 'shop/'.$url;
    }

    public function scopeSort($q)
    {
        if (request('sort') == 'inc_price') {
            $q->orderBy('price', 'asc');
        }
        if (request('sort') == 'dec_price') {
            $q->orderBy('price', 'desc');
        }
        if (request('sort') == 'inc_view') {
            $q->orderBy('view', 'asc');
        }
        if (request('sort') == 'dec_view') {
            $q->orderBy('view', 'desc');
        }

        return $q;
    }

    public function scopeFilter($q)
    {
        if (request('price')) {
            $q->whereBetween('base_price', request('price'));
        }
        if (request('brands')) {
            $q->whereIn('product_line_id', request('brands'));
        }
        return $q;
    }
}
