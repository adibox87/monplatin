<?php

namespace App\Models\Slider;

use App\Models\Image\Image;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Database\Eloquent\Model;

class Slide extends Model
{

    const PHOTO = 'photo';

    protected $fillable = [
        'text',
        'url'
    ];

    public function slider()
    {
        return $this->belongsTo(Slider::class);
    }

    public function image(): MorphOne
    {
        return $this->morphOne(Image::class, 'imageable');
    }
}
