<?php

namespace App\Models\Slider;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    const PHOTO = 'photo';

    protected $fillable = [
        'title',
    ];

    protected $casts = [
        'pages' => 'array',
    ];

    const PAGE_MAIN = 'main';
    const PAGE_CATALOG = 'catalog';

    const PAGES = [
        self::PAGE_MAIN,
        self::PAGE_CATALOG,
    ];

    public function slides()
    {
        return $this->hasMany(Slide::class);
    }
}
