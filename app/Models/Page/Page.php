<?php

namespace App\Models\Page;

use Illuminate\Database\Eloquent\Model;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class Page extends Model
{
    use HasSlug;

    protected $fillable = [
        'slug',
        'seo_title',
        'seo_description',
        'content',
        'title',
        'active',
        'order',
    ];

    public function getSlugOptions()
    {
        return SlugOptions::create()->generateSlugsFrom('title')->saveSlugsTo('slug');
    }
}
