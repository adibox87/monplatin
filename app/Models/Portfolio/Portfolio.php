<?php

namespace App\Models\Portfolio;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Portfolio extends Model
{
    const TYPE_VIDEO = 'video';
    const TYPE_PHOTO = 'photo';

    protected $fillable = [
        'title',
        'image',
        'address',
        'type',
    ];

    public function portfolioCategory()
    {
        return $this->belongsTo('App\Models\Portfolio\PortfolioCategory');
    }

    public function getImageSrcAttribute()
    {
        if (Str::startsWith($this->image, ['http']))
            return $this->image;

        if ($this->type === self::TYPE_VIDEO) {
            if (Str::contains($this->address, 'youtube')) {
                preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $this->address, $match);
                return "http://i3.ytimg.com/vi/${match[1]}/maxresdefault.jpg";
            }

            return url('images/youtube.png');
        }

        return \Voyager::image($this->image);
    }
}
