<?php

namespace App\Models\Portfolio;

use Illuminate\Database\Eloquent\Model;

class PortfolioCategory extends Model
{
    public function portfolios()
    {
        return $this->hasMany('App\Models\Portfolio\Portfolio');
    }
}
