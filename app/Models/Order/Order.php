<?php

namespace App\Models\Order;

use App\Mail\OrderReceivedMail;
use App\Models\Customer\Customer;
use App\Models\Salon\Salon;
use Illuminate\Database\Eloquent\Model;
use PDF;

class Order extends Model
{
    protected $fillable = [
        'customer_id',
        'email',
        'cart',
        'conditions',
        'address',
        'subtotal',
        'total',
        'ip_address',
        'salon_id',
    ];

    protected $casts = [
        'address' => 'json',
        'cart' => 'collection',
        'conditions' => 'collection',
        'subtotal' => 'decimal:2',
        'total' => 'decimal:2',
    ];

    public $additional_attributes = ['pdf_url'];

    protected static function boot()
    {
        self::created(function (self $order) {
            \Mail::to($order->email)->send(new OrderReceivedMail($order));

            $pdf = PDF::loadView('checkout.pdf', compact('order'));


            \Mail::raw('Новый заказ, на сайте, вся информация в прикрепленном файле', function ($message) use ($pdf) {
                $message->subject('Новый заказ');
                $message->to('monplatincenter@mail.ru')->cc(['monplatin9@mail.ru','monpatin.test@gmail.com']);
                $message->attachData($pdf->output(), "new_order.pdf");

            });
        });

        parent::boot();
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id');
    }

    public function salon()
    {
        return $this->belongsTo(Salon::class, 'salon_id');
    }

    public function getPdfUrlAttribute()
    {
        return \URL::signedRoute('checkout.pdf', $this);
    }

    public function getCartBrowseAttribute()
    {
        return $this->pdf_url;
    }
}
