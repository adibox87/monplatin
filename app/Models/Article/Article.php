<?php

namespace App\Models\Article;

use App\Models\Image\Image;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Database\Eloquent\Model;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use TCG\Voyager\Traits\Resizable;

class Article extends Model
{
    use Resizable;
    use HasSlug;

    const PHOTO = 'photo';

    public function getSlugOptions()
    {
        return SlugOptions::create()->generateSlugsFrom('title')->saveSlugsTo('slug');
    }

    public function articleCategory()
    {
        return $this->belongsTo(ArticleCategory::class);
    }

    protected $fillable = [
        'slug',
        'title',
        'small_desc',
        'description',
    ];

    public function photo(): MorphOne
    {
        return $this->morphOne(Image::class, 'model');
    }

    public function scopeFilter($q)
    {
        if (request('filter') == 'inc_date') {
            $q->orderBy('created_at', 'asc');
        }
        if (request('filter') == 'dec_date') {
            $q->orderBy('created_at', 'desc');
        }

        return $q;
    }
}
