<?php

namespace App\Models\Salon;

use Illuminate\Database\Eloquent\Model;

class SalonServiceRecord extends Model
{

    protected $fillable = [
        'name',
        'phone',
        'date_time',
        'salon_service_id',
    ];


//protected static function boot()
//    {
//        self::created(function (self $service) {
//
//            $serviceName = SalonService::find($service->salon_service_id)->title;
//
//
//
//            \Mail::raw('Новый заказ на услугу ' . $serviceName . ' от' . $service->name . $service->phone, function ($message) {
//                $message->subject('Новый заказ на услугу');
//                $message->to('monplatincenter@mail.ru')->cc(['monplatin9@mail.ru','monpatin.test@gmail.com']);
//
//            });
//        });
//
//        parent::boot();
//    }



    public function service()
    {
        return $this->belongsTo(SalonService::class);
    }
}
