<?php

namespace App\Models\Salon;

use Illuminate\Database\Eloquent\Model;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class SalonService extends Model
{
    use HasSlug;

    protected $fillable = [
        'slug',
        'title',
        'seo_title',
        'seo_description',
        'salon_id',
        'small_description',
        'description',
        'salon_service_category_id',
    ];

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function salonServiceCategory()
    {
        return $this->belongsTo(SalonServiceCategory::class);
    }

    public function salons()
    {
        return $this->belongsToMany(Salon::class, 'salons_salon_services', 'salon_service_id', 'salon_id');
    }

    public function records()
    {
        return $this->hasMany(SalonServiceRecord::class);
    }

    public function getSlugOptions()
    {
        return SlugOptions::create()->generateSlugsFrom('title')->saveSlugsTo('slug');
    }
}
