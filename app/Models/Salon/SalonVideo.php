<?php

namespace App\Models\Salon;

use Illuminate\Database\Eloquent\Model;

class SalonVideo extends Model
{

    protected $fillable = [
        'title',
        'url',
    ];

    public function salon()
    {
        return $this->belongsTo(Salon::class);
    }
}
