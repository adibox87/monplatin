<?php

namespace App\Models\Salon;

use Illuminate\Database\Eloquent\Model;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class SalonImage extends Model
{

    protected $fillable = [
        'image',
        'title',
    ];

    public function salon()
    {
        return $this->belongsTo(Salon::class);
    }
}
