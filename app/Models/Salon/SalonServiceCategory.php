<?php

namespace App\Models\Salon;

use Illuminate\Database\Eloquent\Model;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class SalonServiceCategory extends Model
{
    use HasSlug;

    protected $with = [
        'salonServices',
    ];

    protected $fillable = [
        'slug',
        'title',
        'seo_title',
        'seo_description',
        'small_description',
        'description',
        'work_time',
    ];

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function salonServices()
    {
        return $this->hasMany(SalonService::class);
    }

    public function getSalonsAttribute()
    {
        return $this->salonServices
            ->flatMap(fn (SalonService $salonService) => $salonService->salons)
            ->unique('id');
    }

    /**
     * @deprecated
     * @return mixed
     */
    public function getMinServicePrice()
    {
        return $this->salonServices()->get('price')->min() ?: '-';
    }

    public function getMinPriceAttribute()
    {
        return $this->salonServices->min('price');
    }

    public function getSlugOptions()
    {
        return SlugOptions::create()->generateSlugsFrom('title')->saveSlugsTo('slug');
    }
}
