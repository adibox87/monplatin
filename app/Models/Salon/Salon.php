<?php

namespace App\Models\Salon;

use App\Models\Order\Order;
use Illuminate\Database\Eloquent\Model;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class Salon extends Model
{
    use HasSlug;

    protected $fillable = [
        'slug',
        'title',
        'seo_title',
        'seo_description',
        'image',
        'small_description',
        'description',
        'work_time',
    ];

    public function getSlugOptions()
    {
        return SlugOptions::create()->generateSlugsFrom('title')->saveSlugsTo('slug');
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function salonImages()
    {
        return $this->hasMany(SalonImage::class);
    }

    public function salonServices()
    {
        return $this
            ->belongsToMany(SalonService::class, 'salons_salon_services', 'salon_id', 'salon_service_id')
            ->orderBy('sort');
    }

    public function getSalonServiceCategoriesAttribute()
    {
        return $this->salonServices
            ->map(fn (SalonService $salonService) => $salonService->salonServiceCategory)
            ->unique(fn (SalonServiceCategory $salonServiceCategory) => $salonServiceCategory->id)
            ->sortBy(fn (SalonServiceCategory $salonServiceCategory) => $salonServiceCategory->sort);
    }

    public function salonVideos()
    {
        return $this->hasMany(SalonVideo::class);
    }

    public function salonUsers()
    {
        return $this->hasMany(SalonUser::class);
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }
}
