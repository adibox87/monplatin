<?php

namespace App\Models\Salon;

use Illuminate\Database\Eloquent\Model;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class SalonUser extends Model
{
    use HasSlug;

    protected $fillable = [
        'name',
        'phone',
    ];

    public function salon()
    {
        return $this->belongsTo(Salon::class);
    }

    public function getSlugOptions()
    {
        return SlugOptions::create()->generateSlugsFrom('name')->saveSlugsTo('slug');
    }
}
