<?php
namespace App\Providers;
use App\Models\Article\Article;
use App\Models\Category\Category;
use App\Models\Product\ProductLine;
use App\Models\Salon\Salon;
use App\Models\Salon\SalonService;
use App\Models\Salon\SalonServiceCategory;
use Illuminate\Support\ServiceProvider;
use Illuminate\View\View;

class ViewComposerServiceProvider extends ServiceProvider
{
    public function boot()
    {
        view()->composer('partials.categories_tree', function(View $view) {
            $categories = Category::all();
            $stocks = Article::whereHas('articleCategory', function ($q) {
                $q->where('slug', 'akcii-magazina');
            })->take(4)->get();
            $brands = ProductLine::all();
            $view->with('categories', $categories)->with('stocks', $stocks)->with('brands', $brands);
        });

        view()->composer('layouts.app', function(View $view) {
            $salons = Salon::all();
            $view->with('salons', $salons);
        });

        view()->composer('home', function (View $view) {
            $serviceCategories = SalonServiceCategory::query()
                ->with('salonServices')
                ->get();

            $view->with('serviceCategories', $serviceCategories);
        });
    }
}
