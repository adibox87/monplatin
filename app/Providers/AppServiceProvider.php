<?php

namespace App\Providers;

use App\Models\Notification\Notification;
use Carbon\Carbon;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('VoyagerGuard', function () {
            return 'web';
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Carbon::setLocale(config('app.locale'));
        setlocale(LC_TIME, 'ru_RU.utf8');

        if (!$this->app->runningInConsole()) {
            $notifications = Notification::whereDate('valid_till', '>=', now())->first();
            View::share(compact('notifications'));
        }
    }
}
