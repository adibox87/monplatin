<?php

namespace App\Http\Requests\Order;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class OrderCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'country_code' => ['nullable', 'string', Rule::in(['BY'])],
            'state' => ['nullable', 'string'],
            'city' => ['nullable', 'string'],
            'street' => ['nullable', 'string'],
            'street_extra' => ['nullable', 'string'],
            'post_code' => ['nullable', 'string', 'numeric'],

            'salon_id' => ['nullable'],

            'name' => ['required', 'string'],
            'surname' => ['required', 'string'],
            'email' => ['required', 'string', 'email'],
            'phone' => ['required', 'string'],

            'delivery_time' => [
                Rule::requiredIf(fn() => mb_strcasecmp('Минск', $this->request->get('city')) === 0),
                'bail',
                'nullable',
                'string'
            ]
        ];
    }

    public function messages()
    {
        return [
            'delivery_time.required' => 'Выберите время доставки',
        ];
    }

    protected function failedValidation(Validator $validator)
    {

        notify()->error('Проверьте правильность введенных данных', 'Ошибка валидации');
//        notify()->error(collect($validator->getMessageBag()->all())->join(''));

        parent::failedValidation($validator);
    }
}
