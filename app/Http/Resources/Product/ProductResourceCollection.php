<?php

namespace App\Http\Resources\Product;
use Illuminate\Http\Resources\Json\ResourceCollection;

class ProductResourceCollection extends ResourceCollection
{
    public function toArray($request)
    {
        return $this->collection
            ->map
            ->toArray($request, true)
            ->all();
    }

}
