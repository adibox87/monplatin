<?php

namespace App\Http\Resources\Product;

use App\Models\Product\ProductLine;
use Illuminate\Http\Resources\Json\JsonResource as Resource;

class ProductResource extends Resource
{
    public function toArray($request)
    {
        if($this->price) {
            $price = $this->price . ' руб.';
        } else {
            $price = ' по запросу';
        }

        return [
            'id' => $this->id,
            'product_line' => ProductLine::find($this->product_line_id),
            'title' => $this->title,
            'slug' => $this->slug,
            'short_description' => $this->short_description,
            'description' => $this->description,
            'categories' => $this->categories,
            'image' => '/storage/' . $this->image,
            'price' => $price,
            'is_new' => $this->is_new,
            'is_popular' => $this->is_popular,
            'url' => $this->getUrl(),
        ];
    }
}
