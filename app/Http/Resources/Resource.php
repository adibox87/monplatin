<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource as IlluminateResources;

class Resource extends IlluminateResources
{
    protected function addImages($data)
    {
        $resource = $this->resource;

        if (method_exists($resource, 'imagesConfig')) {
            if(!in_array('main_image', $this->except)) {
                $data['main_image'] = $this->main_image_urls;
            }
            if(!in_array('images', $this->except)) {
                $data['images'] = $this->images_urls;
            }
        }

        return $data;
    }

    protected function addMetaToArray($data)
    {
        $data['meta_title'] = $this->generated_meta_title;
        $data['meta_description'] = $this->generated_meta_description;

        return $data;
    }

    protected function addDateToArray($data)
    {
        $data['active_from'] = $this->active_from;
        $data['active_to'] = $this->active_to;

        return $data;
    }

    protected function addCustomAttributesToArray($data)
    {
        return array_merge($data, $this->resource->custom_attributes->toArray());
    }
}
