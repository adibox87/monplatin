<?php

namespace App\Http\Resources\Category;

use Illuminate\Http\Resources\Json\JsonResource as Resource;

class CategoryResource extends Resource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'slug' => $this->slug,
        ];
    }
}
