<?php

namespace App\Http\Resources\Category;
use Illuminate\Http\Resources\Json\ResourceCollection;

class CategoryResourceCollection extends ResourceCollection
{
    public function toArray($request)
    {
        return $this->collection
            ->map
            ->toArray($request, true)
            ->all();
    }

}
