<?php

namespace App\Http\Resources\Salon;
use Illuminate\Http\Resources\Json\ResourceCollection;

class SalonImageResourceCollection extends ResourceCollection
{
    public function toArray($request)
    {
        return $this->collection
            ->map
            ->toArray($request, true)
            ->all();
    }

}
