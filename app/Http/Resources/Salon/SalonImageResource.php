<?php

namespace App\Http\Resources\Salon;

use Illuminate\Http\Resources\Json\JsonResource as Resource;
use Spatie\LaravelImageOptimizer\Facades\ImageOptimizer;

class SalonImageResource extends Resource
{
    public function toArray($request)
    {

        return [
            'id' => $this->id,
            'image' => ImageOptimizer::optimize($this->image),
            'title' => $this->title,
        ];
    }
}
