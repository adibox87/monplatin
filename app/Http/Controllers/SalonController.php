<?php
namespace App\Http\Controllers;

use App\Http\Resources\Salon\SalonImageResource;
use App\Http\Resources\Salon\SalonImageResourceCollection;
use App\Mail\RequestMail;
use App\Mail\ServiceMail;
use App\Models\Article\Article;
use App\Models\Portfolio\Portfolio;
use App\Models\Salon\Salon;
use App\Models\Salon\SalonService;
use App\Models\Salon\SalonServiceCategory;
use App\Models\Salon\SalonServiceRecord;
use App\Models\Slider\Slider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class SalonController extends Controller{

    public function index()
    {
        $salons = Salon::all();

        return view('salons.index', compact('salons'));
    }

    public function view($salon)
    {
        $salon = Salon::where('slug', $salon)->firstOrFail();
        $categories = $salon->salonServiceCategories;

        $portfolios = Portfolio::orderBy('created_at', 'desc')->limit(10)->get();

        return view('salons.show', compact('salon'), compact('categories', 'portfolios'));
    }

    public function record(Request $request)
    {

//        dd($request);

        $request->validate([
            'name' => 'required|max:255',
            'phone' => 'required|max:255',
            'service_salon' => 'required|exists:salons,id',
            'service_id' => 'required|exists:salon_services,id'
        ]);
        $salon = Salon::find($request->service_salon);

        $name = $request->input('name');
        $phone = $request->input('phone');



        $service = SalonService::findOrFail($request->service_id);


        $record = SalonServiceRecord::firstOrCreate(
            [
                'salon_service_id' => $service->id,
                'name' => $name,
                'phone' => $phone,
            ]
        );

        if($record) {
            connectify('успех', 'Ваша заявка принята!', 'В скором времени с Вами свяжется администратор! Спасибо за Ваш выбор!');
            Mail::to('monplatincenter@mail.ru')->send(new ServiceMail($record, $salon));
            Mail::to('monplatin9@mail.ru')->send(new ServiceMail($record, $salon));
            Mail::to('adi.box87@gmail.com')->send(new ServiceMail($record, $salon));


            /*notify()->success('Ваша заявка принята! В скором времени с Вами свяжется администратор! Спасибо за Ваш выбор!');*/
            return redirect()->back()->with('msg-success', 'Спасибо за заявку! В ближайшее время с Вами свяжется специалист, для уточнения деталей!');
        }
    }

}
