<?php

namespace App\Http\Controllers;

use App\Models\Category\Category;
use App\Models\Product\Product;
use Illuminate\Http\Request;

class CatalogController extends Controller
{

    public function index(Request $request)
    {
        $products = Product::sort()->filter()->whereHas('categories')->paginate(12);
        return view('catalog.index', compact('products'));
    }

    public function category($category)
    {
        $category = Category::where('slug', $category)->first();
        $products = Product::sort()->filter()->whereHas('categories', function ($q) use ($category) {
            $q->where('category_id', $category->id);
        })->paginate(12);

        return view('catalog.category', compact('category'), compact('products'));
    }

    public function new(Request $request)
    {
        $products = Product::sort()->filter()->whereHas('categories')->where('new', true)->paginate(12);

        return view('catalog.new', compact('products'));
    }

    public function popular(Request $request)
    {
        $products = Product::sort()->filter()->whereHas('categories')->where('popular', true)->paginate(12);

        return view('catalog.popular', compact('products'));
    }

    public function sale(Request $request)
    {
        $products = Product::sort()->filter()->whereHas('categories')->where('sale_price', '>', 0)->paginate(12);

        return view('catalog.sale', compact('products'));
    }

    public function product($category, $product)
    {
        $category = Category::where('slug', $category)->firstOrFail();
        $product = Product::where('slug', $product)->firstOrFail();
        $product->update([
            'view' => $product->view + 1,
        ]);
        $relatedProducts = $category
            ->products
            ->filter(function (Product $rProduct) use ($product) {
                return $rProduct->isNot($product);
            })
            ->shuffle()
            ->take(4);

        return view('catalog.product', compact('product'), compact('category', 'relatedProducts'));
    }

}
