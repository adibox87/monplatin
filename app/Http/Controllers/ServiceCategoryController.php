<?php

namespace App\Http\Controllers;

use App\Models\Salon\SalonService;
use App\Models\Salon\SalonServiceCategory;

class ServiceCategoryController extends Controller
{
    public function show(SalonServiceCategory $serviceCategory)
    {
        $services = $serviceCategory->salonServices;
        $salons = $serviceCategory->salons;

        return view('service-categories.show', compact('serviceCategory','services', 'salons'));
    }

    public function service(SalonServiceCategory $serviceCategory, SalonService $service)
    {
        return view('service-categories.service', compact('serviceCategory', 'service'));
    }
}
