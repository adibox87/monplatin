<?php


namespace App\Http\Controllers;


use App\Models\Article\ArticleCategory;
use Illuminate\Http\Request;
use App\Models\Page\Page;

class RoutableController
{
    public function show(string $path, Request $request)
    {
        if (($articleCategory = ArticleCategory::where('slug', $path)->first()) != null) {
            $request->route()->name('articles.articles');
            return app(ArticleController::class)->category($path);
        } elseif (($page = Page::where('slug', $path)->first()) != null) {
            $request->route()->name('pages.show');
            return app(PageController::class)->getPage($page);
        }
        return abort('404');
    }
}
