<?php

namespace App\Http\Controllers;

use App\Http\Requests\Order\OrderCreateRequest;
use App\Models\DeliveryOption;
use App\Models\Order\Order;
use App\Models\Salon\Salon;
use PDF;
use Cart;
use Darryldecode\Cart\CartCondition;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class CheckoutController extends Controller
{
    const SHIPPING = 'shipping';

    public function index()
    {
        $salons = Salon::all();
        $deliveryOptions = DeliveryOption::query()
            ->where(fn(Builder $builder) => $builder->whereNull('price_from')->orWhere('price_from', '<=', \Cart::getSubTotal()))
            ->where(fn(Builder $builder) => $builder->whereNull('price_to')->orWhere('price_to', '>', \Cart::getSubTotal()))
            ->get();
        $conditionsId = collect(\Cart::getConditions())->map(fn($condition) => $condition->getAttributes()['id'])->first();
        return view('checkout.index', compact(['deliveryOptions', 'conditionsId', 'salons']));
    }

    public function updateDelivery(Request $request)
    {
        $id = (int)$request->get('method');

        $deliveryOption = DeliveryOption::findOrFail($id);

        $condition = new CartCondition([
            'name' => $deliveryOption->title,
            'type' => self::SHIPPING,
            'target' => 'total',
            'value' => ($deliveryOption->price > 0)
                ? '+' . $deliveryOption->price
                : ($deliveryOption->price < 0
                    ? '-' . $deliveryOption->price
                    : '0'),
            'order' => 1,
            'attributes' => array( // attributes field is optional
                'id' => $deliveryOption->id
            )
        ]);
        Cart::removeConditionsByType(self::SHIPPING);
        Cart::condition($condition);

        return Cart::getTotal();
    }

    public function post(OrderCreateRequest $request)
    {
        $customer = auth('customer')->user();
        $address = $request->validated();

        if (empty(Cart::getConditionsByType(self::SHIPPING))) {
            notify()->error('Необходимо выбрать параметры доставки.');
            return redirect()->back()->withInput();
        }

        $order = new Order([
            'customer_id' => optional($customer)->id,
            'email' => $address['email'],
            'address' => $address,
            'cart' => Cart::getContent(),
            'conditions' => Cart::getConditions(),
            'subtotal' => Cart::getSubTotal(),
            'total' => Cart::getTotal(),
            'ip_address' => $request->ip(),
            'salon_id' => $request->salon_id,
        ]);
        $order->save();
        Cart::clear();


        return redirect(\URL::signedRoute('checkout.view', $order));
    }

    public function view(Order $order)
    {
        return view('checkout.view', compact('order'));
    }

    public function pdf(Order $order)
    {
        $pdf = PDF::loadView('checkout.pdf', compact('order'));
        return $pdf->stream('order.pdf');
    }

    public function mail(Order $order)
    {
        return view('checkout.mail', compact('order'));
    }
}
