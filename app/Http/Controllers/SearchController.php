<?php
namespace App\Http\Controllers;

use App\Models\Article\Article;
use App\Models\Category\Category;
use App\Models\Product\Product;
use App\Models\Product\ProductRecord;
use Illuminate\Http\Request;

class SearchController extends Controller{

    public function index(Request $request)
    {
        $count = Product::whereHas('categories')->where('title', 'like', '%'.$request->input('search').'%')->count();
        $products = Product::whereHas('categories')->where('title', 'like', '%'.$request->input('search').'%')->paginate(12);

        return view('search.index', compact('products'), compact('count'));
    }

}