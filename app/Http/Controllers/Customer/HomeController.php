<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Models\Customer\Customer;
use App\Models\Order\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{

    protected $redirectTo = '/customer/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('customer.auth:customer');
    }

    /**
     * Show the Customer dashboard.
     */
    public function index()
    {
        $customer = Auth::guard('customer')->user();
        return view('customer.home', compact('customer'));
    }

    public function orders() {
        $orders = Order::query()
            ->where('customer_id', \auth('customer')->user()->getAuthIdentifier())
            ->orWhere('email', \auth('customer')->user()->email)
            ->get();
        return view('customer.orders', compact('orders'));
    }

    public function store(Request $request)
    {
        Auth::guard('customer')->user()->update($request->only(['name', 'surname', 'email', 'phone']));
        return redirect()->back()->with('message', 'Ваши личные данные обновлены.');
    }

    public function storeAddress(Request $request)
    {
        /** @var Customer $customer */
        $customer = Auth::guard('customer')->user();
        $new_address = $request->only([
            'country_id',
            'state',
            'street',
            'street_extra',
            'city',
            'post_code',
        ]);
        $customer->hasAddress() ? $customer->updateAddress(($customer->getPrimaryAddress()), $new_address) : $customer->addAddress($new_address);
        return redirect()->back()->with('message', 'Ваш адрес обновлен.');
    }

}
