<?php

namespace App\Http\Controllers;

use App\Models\Portfolio\Portfolio;
use App\Models\Portfolio\PortfolioCategory;
use Illuminate\Http\Request;

class PortfolioController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $portfolios = Portfolio::all();
        $portfolioCategories = PortfolioCategory::all();
        return view('portfolio.portfolio', compact('portfolios', 'portfolioCategories'));
    }
}
