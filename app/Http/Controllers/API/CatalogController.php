<?php

namespace App\Http\Controllers\API;

use App\Http\Resources\Category\CategoryResourceCollection;
use App\Http\Resources\Product\ProductResourceCollection;
use App\Models\Category\Category;
use App\Models\Product\Product;

class CatalogController extends Controller{

    public function categories()
    {
        $categories = Category::all();

        return new CategoryResourceCollection($categories);
    }

    public function products()
    {
        $products = Product::whereHas('categories')->get();

        return new ProductResourceCollection($products);
    }

}