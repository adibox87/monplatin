<?php

namespace App\Http\Controllers;

use App\Models\Article\Article;
use App\Models\Article\ArticleCategory;

class ArticleController extends Controller
{
    public function categories()
    {
        $categories = ArticleCategory::all();

        return view('articles.articleCategories', compact('categories'));
    }

    public function category($categorySlug)
    {
        $category = ArticleCategory::where('slug', $categorySlug)->first();
        if(!$category) return view('404');
        $articles = Article::where('article_category_id', $category->id)->orderByDesc('created_at')->paginate(12);

        return view('articles.articles', compact('articles'), compact('category'));
    }

    public function show($categorySlug, $slug)
    {
        $category = ArticleCategory::where('slug', $categorySlug)->first();
        $article = Article::where('slug', $slug)->first();

        return view('articles.article', compact('article'), compact('category'));
    }
}
