<?php

namespace App\Http\Controllers;

use App\Mail\RequestMail;
use App\Models\Contact\Contact;
use App\Models\Salon\Salon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    public function index()
    {
        $salons = Salon::all();
        return view('contact', compact('salons'));
    }

    public function store(Request $request){
        $name = $request->input('name');
        $phone = $request->input('phone');
        $message = $request->input('message');
        $data =
            [
                'name' => $name,
                'phone' => $phone,
                'message' => $message,
            ];

        $rules = [
            'name' => 'required|max:255',
            'phone' => 'required|max:255',
            'message' => 'required|max:255',
        ];

        $customMessages = [
            'required' => 'Вы не заполнили все поля'
        ];

        /*$services = Service::all();
        $service = $services->where('id', $service_id)->first();*/

        $this->validate($request, $rules, $customMessages);
        $contact = Contact::create($data);
        //$user->notify(new RecordClientNotification($name, $service_id, $number));
        /*Telegram::sendMessage([
            'chat_id' => '304715451',
            'text' => "Новые клиент! Имя: $name. Номер: $number. Услуга: $service->title."
        ]);*/
        connectify('успех', 'Ваша заявка принята!', 'В скором времени с Вами свяжется администратор! Спасибо за Ваш выбор!');
        /*notify()->success('Ваша заявка принята! В скором времени с Вами свяжется администратор! Спасибо за Ваш выбор!');*/
        Mail::to('monplatincenter@mail.ru')->send(new RequestMail($contact));
        Mail::to('yana1husky@gmail.com')->send(new RequestMail($contact));

        return redirect()->back()->with('message', 'Ваша заявка принята! В скором времени
        с Вами свяжется администратор! Спасибо за Ваш выбор!');


    }
}
