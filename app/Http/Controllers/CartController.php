<?php

namespace App\Http\Controllers;

use App\Models\DeliveryOption;
use App\Models\Product\Product;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class CartController extends Controller
{
    public function index()
    {
        $deliveryOptions = DeliveryOption::query()
            ->where(fn(Builder $builder) => $builder->whereNull('price_from')->orWhere('price_from', '<=', \Cart::getSubTotal()))
            ->where(fn(Builder $builder) => $builder->whereNull('price_to')->orWhere('price_to', '>', \Cart::getSubTotal()))
            ->get();
        $conditionsId = collect(\Cart::getConditions())->map(fn($condition) => $condition->getAttributes()['id'])->first();
        return view('cart.index', compact(['deliveryOptions', 'conditionsId']));
    }

    public function add($id, Request $request)
    {
        $product = Product::find($id);
        if ($request->quantity) {
            \Cart::add([
                'id' => $product->id,
                'name' => $product->title,
                'price' => $product->price,
                'quantity' => $request->quantity,
            ]);
        } else {
           \Cart::add([
               'id' => $product->id,
               'name' => $product->title,
               'price' => $product->price,
               'quantity' => 1,
           ]);
       }
        return redirect()->back();
    }

    public function delete($id)
    {
        \Cart::remove($id);
        return redirect()->back();
    }

    public function update(Request $req)
    {
        if (!empty($req->item)) {
            foreach ($req->item as $key => $value) {
                $prev = \Cart::get($key)->quantity;
                $increment = $value - $prev;
                \Cart::update($key, array(
                    'quantity' => $increment
                ));
            }
        }
        return redirect()->back();
    }
}
