<?php

namespace App\Http\Controllers;

use App\Models\Page\Page;

class PageController extends Controller {

    public function index()
    {
        $page = Page::where('slug', '/')->where('active', 1)->first();
        return view('page')->with('page', $page);
    }

    public function getPage(Page $page)
    {
        return view('page')->with('page', $page);
    }

}
