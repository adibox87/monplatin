<?php
namespace App\Http\Controllers;

use App\Models\Article\Article;
use App\Models\Portfolio\Portfolio;
use App\Models\Product\Product;
use App\Models\Salon\Salon;
use App\Models\Slider\Slider;

class HomeController extends Controller {

    public function index()
    {
        $articles = Article::orderBy('created_at', 'desc')->limit(3)->get();
        $slider = Slider::where('pages', 'like', '%'.Slider::PAGE_MAIN.'%')->first();
        $popularProducts = Product::whereHas('categories')->orderBy('view', 'desc')->limit(8)->get();
        $saleProducts = Product::whereHas('categories')->where('sale_price', '>', 0)->limit(8)->get();
        $portfolios = Portfolio::orderBy('created_at', 'desc')->limit(4)->get();
        $salons = Salon::query()
            ->with(['salonServices', 'salonServices.salonServiceCategory'])
            ->get();

        if(!$slider)
            $slides = null;

        $slides = $slider->slides;

        return view('home', compact('articles','slides', 'popularProducts', 'saleProducts', 'portfolios', 'salons'));
    }

}
