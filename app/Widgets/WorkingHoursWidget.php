<?php

namespace App\Widgets;

use App\Models\Salon\Salon;
use Arrilot\Widgets\AbstractWidget;

class WorkingHoursWidget extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $salons = Salon::all();

        return view('widgets.footer.working_hours', compact('salons'));
    }
}
