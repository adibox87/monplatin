<?php


namespace App\Widgets;


use App\Models\Salon\Salon;
use Arrilot\Widgets\AbstractWidget;

class NavbarWidget extends AbstractWidget
{
    public function run()
    {
        $salons = Salon::all();

        return view('widgets._navbar', compact('salons'));
    }
}
