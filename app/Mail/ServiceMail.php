<?php

namespace App\Mail;

use App\Models\Contact\Contact;
use App\Models\Order\Order;
use App\Models\Salon\Salon;
use App\Models\Salon\SalonService;
use App\Models\Salon\SalonServiceRecord;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ServiceMail extends Mailable
{
    use Queueable, SerializesModels;

    public SalonServiceRecord $record;
    public Salon $salon;

    /**
     * Create a new message instance.
     *
     * @param SalonServiceRecord $record
     * @param Salon $salon
     */
    public function __construct(SalonServiceRecord $record, Salon $salon)
    {
        $this->record = $record;
        $this->salon = $salon;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $record = $this->record;
        $salon = $this->salon;
        $service = SalonService::find($record->salon_service_id);

        return $this->subject('Заявка на оказание услуги ' . $service->title)->view('mail.service', compact('service'));
    }
}
